within Magnetic_DCMachines.BaseClasses;
partial model DCMachine "Partial model for DC machines"
  import Modelica.Constants.pi;
  parameter Integer p = 1 "Number of pole pairs";
  parameter Modelica.Units.SI.Temperature TaOperational(start=293.15)
    "Operational armature temperature" annotation (Dialog(group=
          "Operational temperatures", enable=not useThermalPort));
  parameter Modelica.Units.SI.Voltage VaNominal(start=100)
    "Nominal armature voltage" annotation (Dialog(tab="Nominal parameters"));
  parameter Modelica.Units.SI.Current IaNominal(start=100)
    "Nominal armature current (>0..Motor, <0..Generator)"
    annotation (Dialog(tab="Nominal parameters"));
  parameter Modelica.Units.SI.AngularVelocity wNominal(displayUnit="rev/min",
      start=1425*2*pi/60) "Nominal speed"
    annotation (Dialog(tab="Nominal parameters"));
  parameter Modelica.Units.SI.Temperature TaNominal(start=293.15)
    "Nominal armature temperature" annotation (Dialog(tab="Nominal parameters"));
  parameter Modelica.Units.SI.Resistance Ra(start=0.05)
    "Armature resistance at TaRef" annotation (Dialog(tab="Armature"));
  parameter Modelica.Units.SI.Temperature TaRef(start=293.15)
    "Reference temperature of armature resistance"
    annotation (Dialog(tab="Armature"));
  parameter Modelica.Electrical.Machines.Thermal.LinearTemperatureCoefficient20 alpha20a(start=0) "Temperature coefficient of armature resistance" annotation (Dialog(tab="Armature"));
  parameter Modelica.Units.SI.Inductance La(start=0.0015) "Armature inductance"
    annotation (Dialog(tab="Armature"));
  extends Modelica.Electrical.Machines.Interfaces.PartialBasicMachine(
    Jr(start=0.15),
    frictionParameters(wRef=wNominal),
    friction(final useHeatPort=true));
  extends Modelica.Electrical.Machines.Icons.FundamentalWaveMachine;
  parameter Modelica.Electrical.Machines.Losses.CoreParameters coreParameters(final m=1, VRef=ViNominal, wRef=wNominal) "Armature core loss parameter record" annotation (Dialog(tab="Losses"));
  parameter Modelica.Electrical.Machines.Losses.StrayLoadParameters strayLoadParameters(IRef=IaNominal, wRef=wNominal) "Stray load loss parameter record" annotation (Dialog(tab="Losses"));
  parameter Modelica.Electrical.Machines.Losses.BrushParameters brushParameters(ILinear=0.01*IaNominal) "Brush loss parameter record" annotation (Dialog(tab="Losses"));
  replaceable output Modelica.Electrical.Machines.Interfaces.DCMachines.PartialPowerBalanceDCMachines powerBalance(
    final powerArmature=va*ia,
    final powerMechanical=wMechanical*tauShaft,
    final powerInertiaStator=inertiaStator.J*inertiaStator.a*inertiaStator.w,
    final powerInertiaRotor=inertiaRotor.J*inertiaRotor.a*inertiaRotor.w,
    final lossPowerArmature=ra.LossPower,
    final lossPowerCore=core.lossPower,
    final lossPowerStrayLoad=strayLoad.lossPower,
    final lossPowerFriction=friction.lossPower,
    final lossPowerBrush=brush.lossPower) "Power balance";

  output Modelica.Units.SI.Voltage va=pin_ap.v - pin_an.v "Armature voltage";
  output Modelica.Units.SI.Current ia(start=0) = pin_ap.i "Armature current";
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_ap "Positive armature pin" annotation (Placement(transformation(extent={{50,110},{70,90}})));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_an "Negative armature pin" annotation (Placement(transformation(extent={{-70,110},{-50,90}})));
  Modelica.Electrical.Analog.Basic.Resistor ra(final R=Ra, final T_ref=TaRef,
    final alpha=Modelica.Electrical.Machines.Thermal.convertAlpha(alpha20a, TaRef),
    final useHeatPort=true) annotation (Placement(transformation(extent={{60,50},{40,
            70}})));
  Modelica.Electrical.Machines.Losses.DCMachines.Brush brush(final brushParameters=brushParameters, final useHeatPort=true) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=180,
        origin={-40,60})));
  Modelica.Electrical.Machines.Losses.DCMachines.StrayLoad strayLoad(final strayLoadParameters=strayLoadParameters, final useHeatPort=true) annotation (Placement(transformation(extent={{90,50},{70,70}})));
  replaceable Modelica.Electrical.Machines.Interfaces.DCMachines.PartialThermalPortDCMachines thermalPort if useThermalPort annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));
  replaceable Modelica.Electrical.Machines.Interfaces.DCMachines.PartialThermalAmbientDCMachines thermalAmbient(final useTemperatureInputs=false, final Ta=TaOperational) if not useThermalPort annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-30,-80})));
  BasicMachines.Components.Commutator commutator(final p=p, final effectiveTurns=Na) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-10,20})));
  Modelica.Magnetic.FundamentalWave.Components.Ground groundArmature annotation (Placement(transformation(extent={{20,-10},{40,10}})));
  BasicMachines.Components.AirGap airGap(final p=p, final G_ma=La/Na^2)
                                                    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-10,-20})));
  Modelica.Magnetic.FundamentalWave.Components.Ground groundExcitation annotation (Placement(transformation(extent={{20,-10},{40,-30}})));
  parameter Real Na = ViNominal / (p * wNominal * PhiNominal) "Effective number of armature turns";
  Losses.DCMachines.Core core(final G = coreParameters.GcRef * Na^2, final useHeatPort=true) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-30,0})));
protected
  constant Boolean quasiStationary=false "No electrical transients if true" annotation (Evaluate=true);
  parameter Modelica.Units.SI.Voltage ViNominal "Nominal induced Voltage";
  parameter Modelica.Units.SI.MagneticFlux PhiNominal "Nominal magnetic flux";
  replaceable Modelica.Electrical.Machines.Interfaces.DCMachines.PartialThermalPortDCMachines internalThermalPort annotation (Placement(transformation(extent={{-4,-84},{4,-76}})));
equation
  assert(ViNominal > Modelica.Constants.eps, "VaNominal has to be > (Ra[+Rse])*IaNominal");
  connect(brush.n, pin_an) annotation (Line(points={{-50,60},{-60,60},{-60,100}}, color={0,0,255}));
  connect(pin_ap, strayLoad.p) annotation (Line(points={{60,100},{90,100},{90,60}}, color={0,0,255}));
  connect(strayLoad.support, internalSupport) annotation (Line(points={{80,50},{80,38},{60,38},{60,-100}}));
  connect(brush.heatPort, internalThermalPort.heatPortBrush) annotation (Line(points={{-30,50},{-30,40},{50,40},{50,-78.4},{0,-78.4}}, color={191,0,0}));
  connect(strayLoad.heatPort, internalThermalPort.heatPortStrayLoad) annotation (Line(points={{90,50},{90,40},{50,40},{50,-80},{0.4,-80}}, color={191,0,0}));
  connect(friction.heatPort, internalThermalPort.heatPortFriction) annotation (Line(points={{80,-50},{50,-50},{50,-80.8},{0.4,-80.8}}, color={191,0,0}));
  connect(ra.heatPort, internalThermalPort.heatPortArmature) annotation (Line( points={{50,50},{50,-79.2},{-0.4,-79.2}}, color={191,0,0}));
  connect(inertiaRotor.flange_b, strayLoad.flange) annotation (Line(points={{90,0},{90,30},{100,30},{100,70},{80,70}}));
  connect(thermalAmbient.thermalPort, internalThermalPort) annotation (Line(points={{-20,-80},{-12,-80},{0,-80}}, color={191,0,0}));
  connect(internalThermalPort, thermalPort) annotation (Line(points={{0,-80},{0,-100}}, color={191,0,0}));
  connect(commutator.pin_n, brush.p) annotation (Line(points={{-20,30},{-20,60},{-30,60}}, color={0,0,255}));
  connect(commutator.pin_p, ra.n) annotation (Line(points={{1.77636e-15,30},{0,30},{0,60},{40,60}}, color={0,0,255}));
  connect(commutator.flange, strayLoad.flange) annotation (Line(points={{0,20},{90,20},{90,30},{100,30},{100,70},{80,70}}, color={0,0,0}));
  connect(commutator.support, internalSupport) annotation (Line(points={{-20,20},{-30,20},{-30,38},{60,38},{60,-100}}, color={0,0,0}));
  connect(airGap.support, internalSupport) annotation (Line(points={{0,-20},{60,-20},{60,-100}},               color={0,0,0}));
  connect(airGap.flange_a, inertiaRotor.flange_a) annotation (Line(points={{-20,-20},{-52,-20},{-52,36},{70,36},{70,1.77636e-15}}, color={0,0,0}));
  connect(commutator.port_p, groundArmature.port_p) annotation (Line(points={{0,10},{30,10}}, color={255,128,0}));
  connect(airGap.port_ep, groundExcitation.port_p) annotation (Line(points={{0,-30},{30,-30}}, color={255,128,0}));
  connect(ra.p, strayLoad.n) annotation (Line(points={{60,60},{70,60}}, color={0,0,255}));
  connect(commutator.port_p,airGap.port_an)  annotation (Line(points={{0,10},{12,10},{12,-10},{0,-10}}, color={255,128,0}));
  connect(core.port_p, commutator.port_n) annotation (Line(points={{-30,10},{-20,10}}, color={255,128,0}));
  connect(core.port_n,airGap.port_ap)  annotation (Line(points={{-30,-10},{-20,-10}}, color={255,128,0}));
  connect(core.heatPort, internalThermalPort.heatPortCore) annotation (Line(points={{-40,10},{-40,-58},{2,-58},{2,-80},{0.4,-80},{0.4,-79.2}}, color={191,0,0}));
  annotation (Documentation(info="<html>
Partial model for DC machine models.
</html>"),
       Icon(graphics={Line(points={{-40,70},{-60,70},{-60,90}}, color={0,0,255}),
               Line(points={{40,70},{60,70},{60,90}}, color={0,0,255})}));
end DCMachine;
