within Magnetic_DCMachines.Examples.DCMachines;
model DCSE_characteristic
  "Test example: torque-speed characteristic of DC with series excitation"

  extends Modelica.Icons.Example;
  Magnetic_DCMachines.BasicMachines.DCMachines.DC_SeriesExcited dcse(
    VaNominal=dcseData.VaNominal,
    IaNominal=dcseData.IaNominal,
    wNominal=dcseData.wNominal,
    TaNominal=dcseData.TaNominal,
    Ra=dcseData.Ra,
    TaRef=dcseData.TaRef,
    La=dcseData.La,
    Jr=dcseData.Jr,
    useSupport=false,
    Js=dcseData.Js,
    frictionParameters=dcseData.frictionParameters,
    coreParameters=dcseData.coreParameters,
    strayLoadParameters=dcseData.strayLoadParameters,
    brushParameters=dcseData.brushParameters,
    TaOperational=293.15,
    alpha20a=dcseData.alpha20a,
    phiMechanical(fixed=true),
    wMechanical(fixed=false),
    ia(fixed=false),
    Rs=dcseData.Rs,
    TsRef=dcseData.TsRef,
    alpha20s=dcseData.alpha20s,
    Ls=dcseData.Ls,
    sigmas=dcseData.sigmas,
    useSaturation=dcseData.useSaturation,
    Lszer=dcseData.Lszer,
    Lsinf=dcseData.Lsinf,
    TsNominal=dcseData.TsNominal,
    TsOperational=293.15) annotation (Placement(transformation(extent={{-20,-10},{0,10}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=dcseData.VaNominal)
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-60,0})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        transformation(
        origin={-40,-20},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Mechanics.Rotational.Sources.Speed loadSpeed(
    useSupport=false,
    exact=true,
    phi(fixed=false))
    annotation (Placement(transformation(extent={{60,-10},{40,10}})));
  parameter Utilities.ParameterRecords.DcSeriesExcitedData dcseData annotation (Placement(transformation(extent={{-20,-40},{0,-20}})));
  Modelica.Electrical.Analog.Sensors.MultiSensor electricalMultiSensor
    annotation (Placement(transformation(extent={{-50,30},{-30,50}})));
  Modelica.Mechanics.Rotational.Sensors.MultiSensor mechanicalMultiSensor
    annotation (Placement(transformation(extent={{10,-10},{30,10}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=1.5*dcseData.wNominal,
    duration=1,
    offset=0.5*dcseData.wNominal,
    startTime=0)
    annotation (Placement(transformation(extent={{90,-10},{70,10}})));
equation
  connect(constantVoltage.n, ground.p)
    annotation (Line(points={{-60,-10},{-40,-10}}, color={0,0,255}));
  connect(dcse.flange, mechanicalMultiSensor.flange_a)
    annotation (Line(points={{0,0},{10,0}}, color={0,0,0}));
  connect(mechanicalMultiSensor.flange_b, loadSpeed.flange)
    annotation (Line(points={{30,0},{40,0}}, color={0,0,0}));
  connect(constantVoltage.p, electricalMultiSensor.pc)
    annotation (Line(points={{-60,10},{-60,40},{-50,40}}, color={0,0,255}));
  connect(dcse.pin_ap, electricalMultiSensor.nc)
    annotation (Line(points={{-4,10},{-4,40},{-30,40}}, color={0,0,255}));
  connect(electricalMultiSensor.pc, electricalMultiSensor.pv)
    annotation (Line(points={{-50,40},{-50,50},{-40,50}}, color={0,0,255}));
  connect(ground.p, electricalMultiSensor.nv)
    annotation (Line(points={{-40,-10},{-40,30},{-40,30}}, color={0,0,255}));
  connect(dcse.pin_an, dcse.pin_sp)
    annotation (Line(points={{-16,10},{-20,10},{-20,6}}, color={0,0,255}));
  connect(ground.p, dcse.pin_sn)
    annotation (Line(points={{-40,-10},{-20,-10},{-20,-6}}, color={0,0,255}));
  connect(ramp.y, loadSpeed.w_ref)
    annotation (Line(points={{69,0},{62,0}}, color={0,0,127}));
  annotation (experiment(
      StopTime=1.2,
      Interval=0.0001,
      Tolerance=1e-06),                                                Documentation(
        info="<html>
<p>
<strong>Test example: Torque-speed characteristic of a series excited DC machine</strong>
</p>
<p>
The machine is driven by a speed ramp from 0.5 times nominal speed to 2.5 times nominal speed.. 
Plot armature current <code>dcee.ia</code> and electrical torque <code>dcee.tauElectrical</code>
versus angular velocity <code>mechanicalMultiSensor.w</code>.
</p>
<h4>Note:</h4>
<p>
With an ideal series excited machine, angular velocity goes to infinite when load torque is removed completely. 
Therefore it is advisable to use a prescribed angular velocity as mechanical load.
</p>
<p>
Note that the difference between electrical torque and shaft torque is needed to accelerate the inertia of the machine's rotor,
and acceleration is rather high.
</p>
<p>
Default machine parameters of model <em>DC_SeriesExcited</em> are used.
</p>
</html>"));
end DCSE_characteristic;
