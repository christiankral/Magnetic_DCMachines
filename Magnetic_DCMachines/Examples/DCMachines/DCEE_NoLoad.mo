within Magnetic_DCMachines.Examples.DCMachines;
model DCEE_NoLoad "Test example: No-load characteristic of DC machine with electrical excitation"

  extends Modelica.Icons.Example;
  Magnetic_DCMachines.BasicMachines.DCMachines.DC_ElectricalExcited dcee(
    VaNominal=dceeData.VaNominal,
    IaNominal=dceeData.IaNominal,
    wNominal=dceeData.wNominal,
    TaNominal=dceeData.TaNominal,
    Ra=dceeData.Ra,
    TaRef=dceeData.TaRef,
    La=dceeData.La,
    Jr=dceeData.Jr,
    useSupport=false,
    Js=dceeData.Js,
    frictionParameters=dceeData.frictionParameters,
    coreParameters=dceeData.coreParameters,
    strayLoadParameters=dceeData.strayLoadParameters,
    brushParameters=dceeData.brushParameters,
    IeNominal=dceeData.IeNominal,
    Re=dceeData.Re,
    TeRef=dceeData.TeRef,
    Le=dceeData.Le,
    sigmae=dceeData.sigmae,
    TaOperational=293.15,
    alpha20a=dceeData.alpha20a,
    phiMechanical(fixed=true),
    wMechanical(fixed=false),
    ia(fixed=false),
    alpha20e=dceeData.alpha20e,
    useSaturation=dceeData.useSaturation,
    Lezer=dceeData.Lezer,
    Leinf=dceeData.Leinf,
    TeOperational=293.15,
    ie(fixed=false)) annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  parameter Utilities.ParameterRecords.DcElectricalExcitedData dceeData(useSaturation=true) annotation (Placement(transformation(extent={{-10,-40},{10,-20}})));
  Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed(useSupport=false, w_fixed=
        dceeData.w0)
    annotation (Placement(transformation(extent={{40,-10},{20,10}})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        transformation(
        origin={-30,-20},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Electrical.Analog.Sources.RampCurrent rampCurrent(
    I=1.2*dceeData.IeNominal,
    duration=1,
    offset=0,
    startTime=0) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-70,0})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor
    annotation (Placement(transformation(extent={{10,50},{-10,30}})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{-60,20},{-40,0}})));
equation
  connect(ground.p, rampCurrent.p)
    annotation (Line(points={{-30,-10},{-70,-10}}, color={0,0,255}));
  connect(dcee.flange, constantSpeed.flange)
    annotation (Line(points={{10,0},{20,0}}, color={0,0,0}));
  connect(dcee.pin_an, voltageSensor.n) annotation (Line(points={{-6,10},
          {-6,20},{-10,20},{-10,40}}, color={0,0,255}));
  connect(dcee.pin_ap, voltageSensor.p) annotation (Line(points={{6,10},{6,20},{10,20},{10,40}},
                                   color={0,0,255}));
  connect(ground.p, dcee.pin_en) annotation (Line(points={{-30,-10},{
          -20,-10},{-20,-6},{-10,-6}}, color={0,0,255}));
  connect(rampCurrent.n, currentSensor.p)
    annotation (Line(points={{-70,10},{-60,10}}, color={0,0,255}));
  connect(currentSensor.n, dcee.pin_ep) annotation (Line(points={{-40,
          10},{-20,10},{-20,6},{-10,6}}, color={0,0,255}));
  connect(dcee.pin_an, ground.p) annotation (Line(points={{-6,10},{-6,
          20},{-30,20},{-30,-10}}, color={0,0,255}));
  annotation (Icon(coordinateSystem(preserveAspectRatio=false)), Diagram(
        coordinateSystem(preserveAspectRatio=false)),
    experiment(Interval=0.0001, Tolerance=1e-06),
    Documentation(info="<html>
<p>
This example demonstrates the no-load test of an electrical excited DC-machine:
</p>
<p>
The machine is driven with constant speed, the excitation current follows a ramp up to 1.2 times nominal exciation current. 
Plot armature voltage <code>voltageSensor.v</code> versus excitation current <code>currentSensor.i</code>.
</p>
<p>
From that characteristic, the excitation inductance <code>Le</code> can be obtained from the following relationship:<br>
<code>
Va = Vi = turnsRatio*Le(ie)*ie*wMechanical<br>
VaNominal - Ra*IaNominal = ViNominal = turnsRatio*Le(IeNominal)*IeNominal*wNominal
</code>
</p>
</html>"));
end DCEE_NoLoad;
