within Magnetic_DCMachines.Examples.DCMachines;
model DCEE_Selfexcitation "Test example: DC with self-excitation"

  extends Modelica.Icons.Example;
  parameter Modelica.Units.SI.Conductance G=dceeData.IaNominal/dceeData.VaNominal
    "Load conductance";
  Magnetic_DCMachines.BasicMachines.DCMachines.DC_ElectricalExcited dcee(
    VaNominal=dceeData.VaNominal,
    IaNominal=dceeData.IaNominal,
    wNominal=dceeData.wNominal,
    TaNominal=dceeData.TaNominal,
    Ra=dceeData.Ra,
    TaRef=dceeData.TaRef,
    La=dceeData.La,
    Jr=dceeData.Jr,
    useSupport=false,
    Js=dceeData.Js,
    frictionParameters=dceeData.frictionParameters,
    coreParameters=dceeData.coreParameters,
    strayLoadParameters=dceeData.strayLoadParameters,
    brushParameters=dceeData.brushParameters,
    IeNominal=dceeData.IeNominal,
    Re=dceeData.Re,
    TeRef=dceeData.TeRef,
    Le=dceeData.Le,
    sigmae=dceeData.sigmae,
    TaOperational=293.15,
    alpha20a=dceeData.alpha20a,
    phiMechanical(fixed=true),
    wMechanical(fixed=false),
    ia(fixed=true),
    alpha20e=dceeData.alpha20e,
    useSaturation=dceeData.useSaturation,
    Lezer=dceeData.Lezer,
    Leinf=dceeData.Leinf,
    TeOperational=293.15,
    ie(fixed=false)) annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        transformation(
        origin={-30,-20},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed(useSupport=false, w_fixed=
        dceeData.w0)
    annotation (Placement(transformation(extent={{80,-10},{60,10}})));
  parameter Utilities.ParameterRecords.DcElectricalExcitedData dceeData(useSaturation=true) annotation (Placement(transformation(extent={{0,-40},{20,-20}})));
  Modelica.Mechanics.Rotational.Sensors.MultiSensor mechanicalMultiSensor
    annotation (Placement(transformation(extent={{30,-10},{50,10}})));
  Modelica.Electrical.Analog.Sensors.MultiSensor electricalMultiSensor
    annotation (Placement(transformation(extent={{-40,10},{-20,30}})));
  Modelica.Electrical.Analog.Basic.VariableConductor conductor annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-50,0})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=G,
    duration=1,
    offset=G*1e-5,
    startTime=0.25)
    annotation (Placement(transformation(extent={{-90,-10},{-70,10}})));
initial equation
  //initialization with remanent flux
  dcee.airGap.Phi_ee.im = 1e-6*dceeData.Le*(1 - dceeData.sigmae)*dceeData.IeNominal;
equation
  connect(dcee.flange, mechanicalMultiSensor.flange_a)
    annotation (Line(points={{20,0},{30,0}}, color={0,0,0}));
  connect(mechanicalMultiSensor.flange_b, constantSpeed.flange)
    annotation (Line(points={{50,0},{60,0}}, color={0,0,0}));
  connect(electricalMultiSensor.nv, ground.p)
    annotation (Line(points={{-30,10},{-30,-10}}, color={0,0,255}));
  connect(ground.p, dcee.pin_en)
    annotation (Line(points={{-30,-10},{0,-10},{0,-6}},     color={0,0,255}));
  connect(dcee.pin_an, dcee.pin_en) annotation (Line(points={{4,10},{-10,10},{-10,
          -10},{0,-10},{0,-6}},     color={0,0,255}));
  connect(electricalMultiSensor.pc, electricalMultiSensor.pv)
    annotation (Line(points={{-40,20},{-40,30},{-30,30}},
        color={0,0,255}));
  connect(electricalMultiSensor.nc, dcee.pin_ap) annotation (Line(
        points={{-20,20},{16,20},{16,10}},
                                         color={0,0,255}));
  connect(dcee.pin_ap, dcee.pin_ep) annotation (Line(points={{16,10},{16,20},{0,
          20},{0,6}},   color={0,0,255}));
  connect(ground.p, conductor.n)
    annotation (Line(points={{-30,-10},{-50,-10}}, color={0,0,255}));
  connect(electricalMultiSensor.pc, conductor.p)
    annotation (Line(points={{-40,20},{-50,20},{-50,10}}, color={0,0,255}));
  connect(ramp.y, conductor.G)
    annotation (Line(points={{-69,0},{-62,0}}, color={0,0,127}));
  annotation (experiment(
      StopTime=1.5,
      Interval=0.0001,
      Tolerance=1e-06),                                                Documentation(
        info="<html>
<p>
<strong>Test example: Electrically separate excited DC machine started with an armature voltage ramp</strong>
</p>
<p>
The machine is driven with constant speed. The excitation winding is connected in parallel to the armature winding.
To start the self-excitation, a small remanent flux has to be initialized. 
To have a distinct intersection between voltage (resistive) characteristic of the excitation winding and the no-load armature voltage characteristic, both versus excitation current, 
saturation has to be taken into account (i.e. a non-linear characteristic no-load armature voltage versus excitation current). 
After proper self-excitation, the load conductance is enlarged which lowers the armature voltage. This in turn lowers the excitation current.
</p>
<p>
Note: If the direction of rotation is reverse, the excitation winding has to be connected anti-parallel to the armature winding.
</p>
<p>
Investigation of self-excitation is not possible with a quasistatic machine model.
</p>
<p>
Default machine parameters of model <em>DC_ElectricalExcited</em> are used.
</p>
</html>"));
end DCEE_Selfexcitation;
