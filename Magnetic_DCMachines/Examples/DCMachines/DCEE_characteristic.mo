within Magnetic_DCMachines.Examples.DCMachines;
model DCEE_characteristic
  "Test example: torque-speed characteristic of DC with shunt excitation"

  extends Modelica.Icons.Example;
  Magnetic_DCMachines.BasicMachines.DCMachines.DC_ElectricalExcited dcee(
    VaNominal=dceeData.VaNominal,
    IaNominal=dceeData.IaNominal,
    wNominal=dceeData.wNominal,
    TaNominal=dceeData.TaNominal,
    Ra=dceeData.Ra,
    TaRef=dceeData.TaRef,
    La=dceeData.La,
    Jr=dceeData.Jr,
    useSupport=false,
    Js=dceeData.Js,
    frictionParameters=dceeData.frictionParameters,
    coreParameters=dceeData.coreParameters,
    strayLoadParameters=dceeData.strayLoadParameters,
    brushParameters=dceeData.brushParameters,
    TaOperational=293.15,
    alpha20a=dceeData.alpha20a,
    phiMechanical(fixed=true),
    wMechanical(fixed=true, start=dceeData.w0),
    ia(fixed=false),
    IeNominal=dceeData.IeNominal,
    Re=dceeData.Re,
    TeRef=dceeData.TeRef,
    alpha20e=dceeData.alpha20e,
    Le=dceeData.Le,
    sigmae=dceeData.sigmae,
    TeOperational=293.15,
    ie(fixed=false)) annotation (Placement(transformation(extent={{-20,-10},{0,10}})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=dceeData.VaNominal)
    annotation (Placement(transformation(extent={{10,10},{-10,-10}},
        rotation=90,
        origin={-60,0})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        transformation(
        origin={-40,-20},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Mechanics.Rotational.Sources.Torque loadTorque(useSupport=false)
    annotation (Placement(transformation(extent={{60,-10},{40,10}})));
  parameter Utilities.ParameterRecords.DcElectricalExcitedData dceeData annotation (Placement(transformation(extent={{-20,-40},{0,-20}})));
  Modelica.Electrical.Analog.Sensors.MultiSensor electricalMultiSensor
    annotation (Placement(transformation(extent={{-50,30},{-30,50}})));
  Modelica.Mechanics.Rotational.Sensors.MultiSensor mechanicalMultiSensor
    annotation (Placement(transformation(extent={{10,-10},{30,10}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=-1.2*dceeData.tauNominal,
    duration=1,
    offset=0,
    startTime=0)
    annotation (Placement(transformation(extent={{90,-10},{70,10}})));
equation
  connect(constantVoltage.n, ground.p)
    annotation (Line(points={{-60,-10},{-40,-10}}, color={0,0,255}));
  connect(dcee.flange, mechanicalMultiSensor.flange_a)
    annotation (Line(points={{0,0},{10,0}}, color={0,0,0}));
  connect(loadTorque.tau, ramp.y)
    annotation (Line(points={{62,0},{69,0}}, color={0,0,127}));
  connect(dcee.pin_en, ground.p)
    annotation (Line(points={{-20,-6},{-40,-6},{-40,-10}}, color={0,0,255}));
  connect(dcee.pin_ep, dcee.pin_ap) annotation (Line(points={{-20,6},{-20,20},{
          -4,20},{-4,10}},      color={0,0,255}));
  connect(mechanicalMultiSensor.flange_b, loadTorque.flange)
    annotation (Line(points={{30,0},{40,0}}, color={0,0,0}));
  connect(ground.p, dcee.pin_an)
    annotation (Line(points={{-40,-10},{-40,10},{-16,10}}, color={0,0,255}));
  connect(constantVoltage.p, electricalMultiSensor.pc)
    annotation (Line(points={{-60,10},{-60,40},{-50,40}}, color={0,0,255}));
  connect(dcee.pin_ap, electricalMultiSensor.nc)
    annotation (Line(points={{-4,10},{-4,40},{-30,40}}, color={0,0,255}));
  connect(electricalMultiSensor.pc, electricalMultiSensor.pv)
    annotation (Line(points={{-50,40},{-50,50},{-40,50}}, color={0,0,255}));
  connect(ground.p, electricalMultiSensor.nv)
    annotation (Line(points={{-40,-10},{-40,30},{-40,30}}, color={0,0,255}));
  annotation (experiment(
      StopTime=1.2,
      Interval=0.0001,
      Tolerance=1e-06),                                                Documentation(
        info="<html>
<p>
<strong>Test example: Torque-speed characteristic of a shunt excited DC machine</strong>
</p>
<p>
The machine is initialized at no-load condition, and subsequently loaded with a torque ramp up to 1.2 times nominal torque. 
Plot armature current <code>dcee.ia</code> and electrical torque <code>dcee.tauElectrical</code>
versus angular velocity <code>mechanicalMultiSensor.w</code>.
</p>
<p>
Note that the difference between electrical torque and shaft torque is needed to decelerate the inertia of the machine's rotor, 
though deceleration is rather small.
</p>
<p>
Default machine parameters of model <em>DC_ElectricalExcited</em> are used.
</p>
</html>"));
end DCEE_characteristic;
