within Magnetic_DCMachines.Examples.DCMachines;
model DCEE_Generator "Test example: DC generator"

  extends Modelica.Icons.Example;
  parameter Modelica.Units.SI.Conductance G=dceeData.IaNominal/dceeData.VaNominal
    "Load conductance";
  Magnetic_DCMachines.BasicMachines.DCMachines.DC_ElectricalExcited dcee1(
    VaNominal=dceeData.VaNominal,
    IaNominal=dceeData.IaNominal,
    wNominal=dceeData.wNominal,
    TaNominal=dceeData.TaNominal,
    Ra=dceeData.Ra,
    TaRef=dceeData.TaRef,
    La=dceeData.La,
    Jr=dceeData.Jr,
    useSupport=false,
    Js=dceeData.Js,
    frictionParameters=dceeData.frictionParameters,
    coreParameters=dceeData.coreParameters,
    strayLoadParameters=dceeData.strayLoadParameters,
    brushParameters=dceeData.brushParameters,
    IeNominal=dceeData.IeNominal,
    Re=dceeData.Re,
    TeRef=dceeData.TeRef,
    Le=dceeData.Le,
    sigmae=dceeData.sigmae,
    TaOperational=293.15,
    alpha20a=dceeData.alpha20a,
    phiMechanical(fixed=true),
    wMechanical(fixed=false),
    ia(fixed=false),
    alpha20e=dceeData.alpha20e,
    useSaturation=dceeData.useSaturation,
    Lezer=dceeData.Lezer,
    Leinf=dceeData.Leinf,
    TeOperational=293.15,
    ie(fixed=false)) annotation (Placement(transformation(extent={{20,40},{40,60}})));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation (Placement(
        transformation(
        origin={-30,30},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed1(useSupport=
       false, w_fixed=dceeData.w0)
    annotation (Placement(transformation(extent={{100,40},{80,60}})));
  parameter Utilities.ParameterRecords.DcElectricalExcitedData dceeData(useSaturation=true) annotation (Placement(transformation(extent={{20,-10},{40,10}})));
  Modelica.Mechanics.Rotational.Sensors.MultiSensor mechanicalMultiSensor1
    annotation (Placement(transformation(extent={{50,40},{70,60}})));
  Modelica.Electrical.Analog.Sensors.MultiSensor electricalMultiSensor1
    annotation (Placement(transformation(extent={{-20,70},{-40,90}})));
  Modelica.Electrical.Analog.Basic.VariableConductor conductor1 annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-50,50})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=100*G,
    duration=1,
    offset=G*1e-5,
    startTime=0)
    annotation (Placement(transformation(extent={{-100,-10},{-80,10}})));
  Magnetic_DCMachines.BasicMachines.DCMachines.DC_ElectricalExcited dcee2(
    VaNominal=dceeData.VaNominal,
    IaNominal=dceeData.IaNominal,
    wNominal=dceeData.wNominal,
    TaNominal=dceeData.TaNominal,
    Ra=dceeData.Ra,
    TaRef=dceeData.TaRef,
    La=dceeData.La,
    Jr=dceeData.Jr,
    useSupport=false,
    Js=dceeData.Js,
    frictionParameters=dceeData.frictionParameters,
    coreParameters=dceeData.coreParameters,
    strayLoadParameters=dceeData.strayLoadParameters,
    brushParameters=dceeData.brushParameters,
    IeNominal=dceeData.IeNominal,
    Re=dceeData.Re,
    TeRef=dceeData.TeRef,
    Le=dceeData.Le,
    sigmae=dceeData.sigmae,
    TaOperational=293.15,
    alpha20a=dceeData.alpha20a,
    phiMechanical(fixed=true),
    wMechanical(fixed=false),
    ia(fixed=false, start=-dceeData.IeNominal),
    alpha20e=dceeData.alpha20e,
    useSaturation=dceeData.useSaturation,
    Lezer=dceeData.Lezer,
    Leinf=dceeData.Leinf,
    TeOperational=293.15,
    ie(fixed=false, start=dceeData.IeNominal)) annotation (Placement(transformation(extent={{20,-60},{40,-40}})));
  Modelica.Electrical.Analog.Basic.Ground ground2
                                                 annotation (Placement(
        transformation(
        origin={-30,-70},
        extent={{-10,-10},{10,10}},
        rotation=0)));
  Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed2(useSupport=
       false, w_fixed=dceeData.w0)
    annotation (Placement(transformation(extent={{100,-60},{80,-40}})));
  Modelica.Mechanics.Rotational.Sensors.MultiSensor mechanicalMultiSensor2
    annotation (Placement(transformation(extent={{50,-60},{70,-40}})));
  Modelica.Electrical.Analog.Sensors.MultiSensor electricalMultiSensor2
    annotation (Placement(transformation(extent={{-20,-30},{-40,-10}})));
  Modelica.Electrical.Analog.Basic.VariableConductor conductor2
                                                               annotation (
      Placement(transformation(
        extent={{10,-10},{-10,10}},
        rotation=90,
        origin={-50,-50})));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=dceeData.Re
        *dceeData.IeNominal) annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={0,50})));
equation
  connect(dcee1.flange, mechanicalMultiSensor1.flange_a)
    annotation (Line(points={{40,50},{50,50}}, color={0,0,0}));
  connect(mechanicalMultiSensor1.flange_b, constantSpeed1.flange)
    annotation (Line(points={{70,50},{80,50}}, color={0,0,0}));
  connect(electricalMultiSensor1.nv, ground1.p)
    annotation (Line(points={{-30,70},{-30,40}}, color={0,0,255}));
  connect(electricalMultiSensor1.pc, electricalMultiSensor1.pv)
    annotation (Line(points={{-20,80},{-20,90},{-30,90}}, color={0,0,255}));
  connect(ground1.p, conductor1.n)
    annotation (Line(points={{-30,40},{-50,40}}, color={0,0,255}));
  connect(dcee2.flange, mechanicalMultiSensor2.flange_a)
    annotation (Line(points={{40,-50},{50,-50}}, color={0,0,0}));
  connect(mechanicalMultiSensor2.flange_b, constantSpeed2.flange)
    annotation (Line(points={{70,-50},{80,-50}}, color={0,0,0}));
  connect(electricalMultiSensor2.nv, ground2.p)
    annotation (Line(points={{-30,-30},{-30,-60}}, color={0,0,255}));
  connect(ground2.p, dcee2.pin_en)
    annotation (Line(points={{-30,-60},{20,-60},{20,-56}}, color={0,0,255}));
  connect(dcee2.pin_an, dcee2.pin_en) annotation (Line(points={{24,-40},{10,-40},
          {10,-60},{20,-60},{20,-56}}, color={0,0,255}));
  connect(electricalMultiSensor2.pc, electricalMultiSensor2.pv)
    annotation (Line(points={{-20,-20},{-20,-10},{-30,-10}}, color={0,0,255}));
  connect(dcee2.pin_ap, dcee2.pin_ep) annotation (Line(points={{36,-40},{36,-30},
          {20,-30},{20,-44}}, color={0,0,255}));
  connect(ground2.p, conductor2.n)
    annotation (Line(points={{-30,-60},{-50,-60}}, color={0,0,255}));
  connect(ground1.p, constantVoltage.n)
    annotation (Line(points={{-30,40},{-1.77636e-15,40}}, color={0,0,255}));
  connect(constantVoltage.p, dcee1.pin_ep) annotation (Line(points={{0,60},{10,
          60},{10,56},{20,56}}, color={0,0,255}));
  connect(ground1.p, dcee1.pin_en) annotation (Line(points={{-30,40},{10,40},{
          10,44},{20,44}}, color={0,0,255}));
  connect(dcee1.pin_an, ground1.p) annotation (Line(points={{24,60},{24,70},{
          -20,70},{-20,40},{-30,40}}, color={0,0,255}));
  connect(ramp.y, conductor1.G) annotation (Line(points={{-79,0},{-70,0},{-70,
          50},{-62,50}}, color={0,0,127}));
  connect(ramp.y, conductor2.G) annotation (Line(points={{-79,0},{-70,0},{-70,
          -50},{-62,-50}}, color={0,0,127}));
  connect(electricalMultiSensor2.nc, conductor2.p)
    annotation (Line(points={{-40,-20},{-50,-20},{-50,-40}}, color={0,0,255}));
  connect(dcee2.pin_ap, electricalMultiSensor2.pc)
    annotation (Line(points={{36,-40},{36,-20},{-20,-20}}, color={0,0,255}));
  connect(electricalMultiSensor1.pc, dcee1.pin_ap)
    annotation (Line(points={{-20,80},{36,80},{36,60}}, color={0,0,255}));
  connect(electricalMultiSensor1.nc, conductor1.p)
    annotation (Line(points={{-40,80},{-50,80},{-50,60}}, color={0,0,255}));
  annotation (experiment(Interval=0.0001, Tolerance=1e-06),            Documentation(
        info="<html>
<p>
<strong>Test example: Compare a separate excited and a shunt excited DC generator</strong>
</p>
<p>
Both machines are driven with constant speed. 
The first one is excited by a constant auxilliary voltage, the second one is shunt excited.
Both machines are loaded by a conductor from (nearly) no-load to overload (which would cause much too hifg armature current in reality.
Plot for both machines armature voltage <code>electricalMultiSensorX.v</code> versus armature current <code>electricalMultiSensorX.i</code>. 
The second machine with shunt excitation reveals a point of maximum current.
</p>
<p>
Default machine parameters of model <em>DC_ElectricalExcited</em> are used.
</p>
</html>"));
end DCEE_Generator;
