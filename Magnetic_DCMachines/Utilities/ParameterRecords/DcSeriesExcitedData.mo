within Magnetic_DCMachines.Utilities.ParameterRecords;
record DcSeriesExcitedData "Common parameters for DC machines"
  extends DcPermanentMagnetData(
    wNominal=1410*2*pi/60,
    ViNominal=VaNominal-
      (Modelica.Electrical.Machines.Thermal.convertResistance(Ra, TaRef, alpha20a, TaNominal)
      +Modelica.Electrical.Machines.Thermal.convertResistance(Rs, TsRef, alpha20s, TsNominal))*IaNominal
      - Modelica.Electrical.Machines.Losses.DCMachines.brushVoltageDrop(brushParameters, IaNominal),
    w0=Modelica.Constants.inf);
  import Modelica.Constants.pi;
  parameter Modelica.Units.SI.Resistance Rs=0.01
    "Series excitation resistance at TsRef"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Temperature TsRef=293.15
    "Reference temperature of series excitation resistance"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Electrical.Machines.Thermal.LinearTemperatureCoefficient20
    alpha20s=0 "Temperature coefficient of series excitation resistance"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Inductance Ls=0.0005
    "Total series excitation inductance" annotation (Dialog(tab="Excitation"));
  parameter Real sigmas(
    min=0,
    max=0.99) = 0 "Stray fraction of series excitation inductance"
    annotation (Dialog(tab="Excitation"));
  parameter Boolean useSaturation=false "Take saturation into account?"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Inductance Lszer=5*Ls
    "Inductance at small current"
    annotation (Dialog(tab="Excitation", enable=useSaturation));
  parameter Modelica.Units.SI.Inductance Lsinf=Ls/5
    "Inductance at large current"
    annotation (Dialog(tab="Excitation", enable=useSaturation));
  parameter Modelica.Units.SI.Temperature TsNominal=293.15
    "Nominal series excitation temperature"
    annotation (Dialog(tab="Nominal parameters"));
  annotation (
    defaultComponentName="dcseData",
    defaultComponentPrefixes="parameter",
    Documentation(info="<html>
<p>Basic parameters of DC machines are predefined with default values.</p>
</html>"));
end DcSeriesExcitedData;
