within Magnetic_DCMachines.Losses.DCMachines;
function coreLossCurrent "Current due to core losses"
  extends Modelica.Icons.Function;
  input Modelica.Electrical.Machines.Losses.CoreParameters coreParameters
    "Core loss parameters";
  input Modelica.Units.SI.AngularVelocity w "Remagnetization angular velocity";
  input Modelica.Units.SI.Voltage v "Actual inner voltage";
  output Modelica.Units.SI.Current i "Current loss";
protected
  Modelica.Units.SI.Conductance Gc "Variable core loss conductance";
//Modelica.SIunits.AngularVelocity wLimit=noEvent(max(noEvent(abs(w)), coreParameters.wMin)) "Limited angular velocity";
algorithm
  if (coreParameters.PRef <= 0) then
    Gc :=0;
    i :=0;
  else
    Gc :=coreParameters.GcRef;
    // * (coreParameters.wRef/wLimit*coreParameters.ratioHysteresis + 1 - coreParameters.ratioHysteresis);
    i :=Gc*v;
  end if;
  annotation (Documentation(info="<html>
<p>
Calculates the current through the core loss conductor, according to
<a href=\"modelica://Modelica.Electrical.Machines.Losses.DCMachines.Core\">Core</a> losses,
e.g., used for initial equations.
</p>
</p>
</html>"));
end coreLossCurrent;
