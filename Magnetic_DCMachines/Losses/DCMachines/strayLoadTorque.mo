within Magnetic_DCMachines.Losses.DCMachines;
function strayLoadTorque "Stray load loss torque"
  extends Modelica.Icons.Function;
  input Modelica.Electrical.Machines.Losses.StrayLoadParameters strayLoadParameters
    "Stray load loss parameters";
  input Modelica.Units.SI.AngularVelocity w "Actual angular velocity";
  input Modelica.Units.SI.Current i "Actual current";
  output Modelica.Units.SI.Torque tau "Stray load loss torque";
algorithm
  if (strayLoadParameters.PRef <= 0) then
    tau :=0;
  else
    tau :=-strayLoadParameters.tauRef*(i/strayLoadParameters.IRef)^2*sign(w)
      *(abs(w)/strayLoadParameters.wRef)^strayLoadParameters.power_w;
  end if;
  annotation (Documentation(info="<html>
<p>
Calculates the torque due to stray load losses, according to
<a href=\"modelica://Modelica.Electrical.Machines.Losses.DCMachines.StrayLoad\">StrayLoad</a> losses,
e.g., used for initial equations.
</p>
</p>
</html>"));
end strayLoadTorque;
