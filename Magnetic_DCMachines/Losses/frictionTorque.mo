within Magnetic_DCMachines.Losses;
function frictionTorque "Friction loss torque"
  extends Modelica.Icons.Function;
  input Modelica.Electrical.Machines.Losses.FrictionParameters frictionParameters
    "Friction loss parameters";
  input Modelica.Units.SI.AngularVelocity w "Actual angular velocity";
  output Modelica.Units.SI.Torque tau "Friction loss torque";
algorithm
  if (frictionParameters.PRef <= 0) then
    tau :=0;
  else
    tau :=-(if noEvent(abs(w) > frictionParameters.wLinear) then
      frictionParameters.tauRef*sign(w)*(abs(w)/frictionParameters.wRef)^
      frictionParameters.power_w else frictionParameters.tauLinear*w/
      frictionParameters.wLinear);
  end if;
  annotation (Documentation(info="<html>
<p>
Calculates the torque due to friction losses, according to
<a href=\"modelica://Modelica.Electrical.Machines.Losses.Friction\">Friction</a> losses,
e.g., used for initial equations.
</p>
</p>
</html>"));
end frictionTorque;
