within Magnetic_DCMachines.Saturation;
package Examples "Examples demonstrating saturation"
  extends Modelica.Icons.ExamplesPackage;

  annotation (Documentation(info="<html>
<p>
Examples demonstrating inductors with saturation
</p>
</html>"));
end Examples;
