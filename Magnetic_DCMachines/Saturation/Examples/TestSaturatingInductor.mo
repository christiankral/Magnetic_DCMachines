within Magnetic_DCMachines.Saturation.Examples;
model TestSaturatingInductor
  "Simple demo to show behaviour of SaturatingInductor component"
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  parameter ParameterRecords.SaturationData saturationData
    annotation (Placement(transformation(extent={{-60,40},{-40,60}})));
  parameter Modelica.Units.SI.MagneticPotentialDifference V_m=2*saturationData.V_mnom
    "Source magnetic voltage(peak)";
  parameter Modelica.Units.SI.MagneticFlux Phi=2*pi*f*saturationData.G_mnom*V_m
    "Source flux (peak)";
  parameter Modelica.Units.SI.Frequency f=10 "Source frequency";
  Components.SaturatingInductor saturatingInductor(Phi(fixed=false),
      saturationData=saturationData) annotation (Placement(transformation(
        origin={70,60},
        extent={{-10,-10},{10,10}},
        rotation=270)));

  Components.SaturatingInductor saturatingInductor1(
      saturationData=saturationData) annotation (Placement(transformation(
        origin={-30,-40},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Components.SaturatingInductor saturatingInductor2(Phi(fixed=false),
      saturationData=saturationData) annotation (Placement(transformation(
        origin={70,-40},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Modelica.Magnetic.FluxTubes.Sources.SignalMagneticPotentialDifference magVoltageSource1 annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={40,60})));
  Modelica.Magnetic.FluxTubes.Sources.SignalMagneticFlux magFluxSource1    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={-60,-40})));
  Modelica.Magnetic.FluxTubes.Sources.SignalMagneticPotentialDifference magVoltageSource annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={40,-40})));
  Modelica.Magnetic.FluxTubes.Basic.Ground ground annotation (Placement(transformation(extent={{30,20},{50,40}})));
  Modelica.Magnetic.FluxTubes.Basic.Ground ground1 annotation (Placement(transformation(extent={{-70,-80},{-50,-60}})));
  Modelica.Magnetic.FluxTubes.Basic.Ground ground2 annotation (Placement(transformation(extent={{30,-80},{50,-60}})));
  Modelica.Blocks.Sources.Ramp ramp(
    height=2*Phi,
    duration=1/f,
    offset=-Phi)                    annotation (Placement(transformation(extent={{0,50},{20,70}})));
  Modelica.Blocks.Sources.Cosine cosine(amplitude=Phi, f=f)
    annotation (Placement(transformation(extent={{-100,-50},{-80,-30}})));
  Modelica.Blocks.Sources.Sine sine(amplitude=V_m, f=f)
    annotation (Placement(transformation(extent={{0,-50},{20,-30}})));
equation
  connect(cosine.y, magFluxSource1.Phi) annotation (Line(
      points={{-79,-40},{-71,-40},{-71,-40}},
      color={0,0,127},
      smooth=Smooth.Bezier));
  connect(ramp.y, magVoltageSource1.V_m) annotation (Line(
      points={{21,60},{29,60},{29,60}},
      color={0,0,127},
      smooth=Smooth.Bezier));
  connect(magVoltageSource1.port_n, ground.port) annotation (Line(points={{40,50},{40,40},{40,40}}, color={255,127,0}));
  connect(ground.port, saturatingInductor.port_n) annotation (Line(points={{40,40},{70,40},{70,50}}, color={255,127,0}));
  connect(magVoltageSource1.port_p, saturatingInductor.port_p) annotation (Line(points={{40,70},{40,80},{70,80},{70,70}}, color={255,127,0}));
  connect(magFluxSource1.port_p, saturatingInductor1.port_p) annotation (Line(points={{-60,-30},{-60,-20},{-30,-20},{-30,-30}}, color={255,127,0}));
  connect(magFluxSource1.port_n, ground1.port) annotation (Line(points={{-60,-50},{-60,-60}}, color={255,127,0}));
  connect(ground1.port, saturatingInductor1.port_n) annotation (Line(points={{-60,-60},{-30,-60},{-30,-50}}, color={255,127,0}));
  connect(magVoltageSource.port_p, saturatingInductor2.port_p) annotation (Line(points={{40,-30},{40,-20},{70,-20},{70,-30}}, color={255,127,0}));
  connect(saturatingInductor2.port_n, ground2.port) annotation (Line(points={{70,-50},{70,-60},{40,-60}}, color={255,127,0}));
  connect(ground2.port, magVoltageSource.port_n) annotation (Line(points={{40,-60},{40,-50}}, color={255,127,0}));
  connect(sine.y, magVoltageSource.V_m) annotation (Line(points={{21,-40},{29,-40}}, color={0,0,127}));
  annotation (
    Diagram(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
            100}})),
    experiment(
      StopTime=0.2,
      Interval=0.0001,
      Tolerance=1e-06),
    Documentation(info="<html>
<p>This simple circuit uses the saturating inductor which has an inductance dependent on current.</p>
<p>Plot <code>saturatingInductor.Psi</code> versus <code>saturatingInductor.i</code></p>
<p>Note that applying a sinusoidal voltage to a saturating inductor causes a current waveform that contains harmonics, expecially a 3rd harmonic.</p>
</html>"));
end TestSaturatingInductor;
