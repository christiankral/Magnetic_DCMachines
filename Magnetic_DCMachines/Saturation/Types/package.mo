within Magnetic_DCMachines.Saturation;
package Types "Types used for saturation"
  extends Modelica.Icons.TypesPackage;

  type PermeanceSlope = Real (
      final quantity="PermeanceSlope",
      final unit="H/s");

  annotation (Documentation(info="<html>
<p>
Define types used for saturation
</p>
</html>"));
end Types;
