within Magnetic_DCMachines.Saturation.UsersGuide;
class Parameterization "Parameterization of saturating inductors"
  extends Modelica.Icons.Information;
  annotation (DocumentationClass=true, Documentation(info="<html>
<p>
The parameters are summarized in the parameter record <code>SaturationData</code>:
</p>
<table>
<tr><td>Type      </td><td>Parameter                       </td><td>Remark</td></tr>
<tr><td>Inductance</td><td><code>L<sub>Nominal</sub></code></td><td>Nominal inductance at nominal current</td></tr>
<tr><td>Boolean   </td><td><code>useSaturation</code>      </td><td>Take saturation into account?</td></tr>
<tr><td>Current   </td><td><code>I<sub>Nominal</sub></code></td><td>Nominal current</td></tr>
<tr><td>Inductance</td><td><code>L<sub>0</sub></code>      </td><td>Inductance at small currents</td></tr>
<tr><td>Inductance</td><td><code>L<sub>&infin;</sub></code></td><td>Inductance at large currents</td></tr>
<tr><td>Current   </td><td><code>I<sub>par</sub></code>    </td><td>Parameter for interpolation, calculated automatically</td></tr>
</table>
<p>
<figure>
<img src=\"modelica://Satan/Resources/Images/Electrical/Machines/Saturation/SaturatingInductor_Lact_i.png\" alt=\"Lact vs. i\">
</figure>
</p>
<h4>Note:</h4>
<p>
If <code>useSaturation = false</code>, the rest of the parameters is ignored, taking a constant inductance <code>L<sub>Nominal</sub></code> into acoount.
</p>
<p>
If <code>useSaturation = true</code>, <code>L<sub>0</sub> &gt; L<sub>Nominal</sub> &gt; L<sub>&infin;</sub></code> has to be fulfilled.
</p>
</html>"));
end Parameterization;
