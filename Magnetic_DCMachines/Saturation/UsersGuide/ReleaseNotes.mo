within Magnetic_DCMachines.Saturation.UsersGuide;
class ReleaseNotes "Release Notes"
  extends Modelica.Icons.ReleaseNotes;
  annotation (Documentation(info="<html>

<h5>Version 1.0.0, 2019-07</h5>
<ul>
<li>First tagged version</li>
</ul>

</html>"));
end ReleaseNotes;
