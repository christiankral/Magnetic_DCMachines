within Magnetic_DCMachines.Saturation;
package UsersGuide "User's Guide"
  extends Modelica.Icons.Information;





  annotation (DocumentationClass=true, Documentation(info="<html>
<p>
This library provides an implementation of magnetic saturation <code>&psi;(i) = L(i)*i</code> without taking hysteresis into account.
</p>
<h4>Saturation characteristics</h4>
<p>
Modeling such a strongly non-linear relationship like saturation, there are two choices:
<table>
<tr><td>Method       </td><td>Example            </td><td>Advantages                                 </td><td>Disadvantages<td></tr>
<tr><td>Interpolation</td><td>cubic splines      </td><td>fits exactly each given pair of values     </td><td>possibley less performant, not so easy to find a rough estimation</td></tr>
<tr><td>Approximation</td><td>polynomial function</td><td>performant, easy to find a rough estimation</td><td>needs optimization to fit measurement data</td></tr>
</table>
<p>
For interpolation and extrpolation some rules should hold to guarantee a meaningful model and performant simulation:
</p>
<ul>
<li><code>&psi;(i)</code> has to be continuously differentiable at least once.</li>
<li><code>L(i)</code> has to be continuously differentiable at least once.</li>
<li><code>&psi;(-i) = -&psi;(i)</code></li>
<li><code>L(-i) = L(i)</code></li>
<li><code>&psi;(i=0) = 0</code></li>
<li><code>lim<sub> i&rarr;0</sub> (d &psi;/ di) = lim<sub> i&rarr;0</sub> (L = &psi;/i) = L<sub>0</sub></code></li>
<li><code>lim<sub> i&rarr;&infin;</sub> (d &psi;/ di) = lim<sub> i&rarr;&infin;</sub> (L = &psi;/i) = L<sub>&infin;</sub> &lt; L<sub>0</sub></code></li>
</ul>
<p>
After investigation of different methods mentioned in the liteature, in this library an appoximaton based on the <code>atan</code>-function is used.
</p>
<p>
This library provides two functions:
</p>
<ul>
<li><code>saturatedInductance</code> calculates <code>L(i)</code></li>
<li><code>saturatedFlux</code> calculates <code>&psi;(i)</code></li>
</ul>
<p>
Since for calculating voltage <code>v = d&psi;/dt = d&psi;/di * di/dt = dL/di * di/dt * i + L * di/dt</code> either the derivative of flux or the derivative of inductance is needed, 
two additional functions are included:
</p>
<ul>
<li><code>derSaturatedInductance</code> calculates <code>dL/di * di/dt</code></li>
<li><code>derSaturatedFlux</code> calculates <code>d&psi;(i)/di * di/dt</code></li>
</ul>
</html>"));
end UsersGuide;
