within Magnetic_DCMachines.Saturation.UsersGuide;
class References "References"
  extends Modelica.Icons.References;
  annotation (Documentation(info="<html>
<p><b>References</b> </p>
<table cellspacing=\"0\" cellpadding=\"2\" border=\"0\"><tr>
<td><p>[Haumer81]</p></td>
<td><p>Anton Haumer, <i>Dynamische Vorg&auml;nge beim Stern-Dreieck-Anlauf von Asynchronmaschinen unter n&auml;herungsweiser Ber&uuml;cksichtigung der S&auml;ttigung</i>, 
Diploarbeit, Technische Universit&auml;t Wien, &Ouml;sterreich, 1981</p></td>
</tr>
<tr>
<td><p>[Beckert80]</p></td>
<td><p>U. Beckert and H. Rieck, <i>Darstellung von Magnetisierungskurven durch kubische Spline-Funtionen</i>, 
Zeitschrift f&uuml; Informations- und Energietechnik 10 (1980), H. 1, pp. 69-73</p></td>
</tr>
<tr>
<td><p>[Deleroi70]</p></td>
<td><p>W. Deleroi, <i>Ber&uuml;cksichtigung der Eisens&auml;ttigung f&uuml;r dynamische Betriebszust&auml;nde</i>, 
Archiv f&uuml; Elektrotechnik Jhg. 1954 (1970), H. 1, pp. 31-42</p></td>
</tr>
<tr>
<td><p>[Fischer 56]</p></td>
<td><p>J. Fischer und H. Moser, <i>Die Nachbildung magnetischer Kennlinien durch einfache algebraische oder transzendente Funktionen</i>, 
Archiv f&uuml; Elektrotechnik Jhg. 1942 (1956), H. 5, pp. 286-299</p></td>
</tr>
<tr>
<td><p>[Fruechtenicht78]</p></td>
<td><p>J. Fr&uuml;chtenicht, <i>Analytische Beschreibung von Magnetisierungskennlinien bei harmonischer Erregung</i>, 
ETZ-A, Bd. 99 (1978), H. 11, pp. 671-674</p></td>
</tr>
</table>
</html>"));
end References;
