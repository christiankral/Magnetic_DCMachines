within Magnetic_DCMachines.Saturation.UsersGuide;
class Concept "Concept of saturation based on atan"
  extends Modelica.Icons.Information;
  annotation (DocumentationClass=true, Documentation(info="<html>
<p>
The approximation function <code>&psi; = L<sub>&infin;</sub>*i + (L<sub>0</sub> - L<sub>&infin;</sub>)*I<sub>par</sub>*atan(i/I<sub>par</sub>)</code> 
fullfills all rules mentioned in the User's Guide.
</p>
<p>
<figure>
<img src=\"modelica://Satan/Resources/Images/Electrical/Machines/Saturation/SaturatingInductor_Psi_i.png\" alt=\"Psi vs. i\">
</figure>
<p>
All parameters (<code>L<sub>0</sub>, L<sub>&infin;</sub>, I<sub>par</sub></code>) are summarized in the parameter record <code>SaturationData</code>, 
Here the parameter <code>I<sub>par</sub></code> is calculated from <code>L(i=I<sub>Nominal</sub>) = L<sub>Nominal</sub></code> by the function <code>iterateParameter</code>. 
which in turn uses the function <code>scaledParameter</code> for nonlinear iteration.
</p>
<h4>Usage:</h4>
<ul>
<li><code>parameter SaturationData saturationData;</code></li>
<li><code>Modelica.SIunits.Current i;</code></li>
<li><code>Modelica.SIunits.Inductance L = saturatedInductance(saturationData, i);</code> //optional</li>
<li><code>Modelica.SIunits.MagneticFlux &psi; = saturatedFlux(saturationData, i);</code></li>
<li><code>Modelica.SIunits.Voltage v = der(&psi;);</code></li>
</ul>
</html>"));
end Concept;
