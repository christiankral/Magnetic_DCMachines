within Magnetic_DCMachines.Saturation;
package ParameterRecords "Parameter records for saturation"
  extends Modelica.Icons.RecordsPackage;

  annotation (Documentation(info="<html>
<p>
Parameter records to define saturation parameters
</p>
</html>"));
end ParameterRecords;
