within Magnetic_DCMachines.Saturation.ParameterRecords;
record SaturationData "Base parameter record for saturation data"
  extends Modelica.Icons.Record;
  import Magnetic_DCMachines.Saturation.Functions.iterateParameter;
  parameter Modelica.Units.SI.Permeance G_mnom=1
    "Nominal permeance at magnetic potential difference V_mnom" annotation (
      Dialog(groupImage=
          "modelica://Magnetic_DCMachines/Resources/Images/Saturation/ParameterRecords/SaturationData/SaturatingPermeance_G_mact_V_m_tight.png"));
  parameter Boolean useSaturation=true "Use saturation, if true";
  parameter Modelica.Units.SI.Current V_mnom=1
    "Nominal magnetic potential difference"
    annotation (Dialog(enable=useSaturation));
  parameter Modelica.Units.SI.Permeance G_mzer=5*G_mnom
    "Permeance at small magnetic potential difference"
    annotation (Dialog(enable=useSaturation));
  parameter Modelica.Units.SI.Permeance G_minf=G_mnom/5
    "Permeance at large magnetic potential difference"
    annotation (Dialog(enable=useSaturation));
  parameter Modelica.Units.SI.Current V_mpar=if useSaturation then
      iterateParameter(
      G_mnom,
      V_mnom,
      G_mzer,
      G_minf) else V_mnom "Result is parameter magnetic potential differene"
    annotation (Dialog(enable=false));
  annotation (defaultComponentPrefixes = "parameter",
  Documentation(info="<html>
<p>
Summarizes parameters for saturation:
</p>
</html>"));
end SaturationData;
