within Magnetic_DCMachines.Saturation.Functions;
function derSaturatedFlux "Caluclates derivative of flux dependent on current"
  extends Modelica.Icons.Function;
  input ParameterRecords.SaturationData saturationData "Parameter record for saturation";
  input Modelica.Units.SI.Current V_m "Actual current";
  input Modelica.Units.SI.CurrentSlope der_V_m "Actual derivative of current";
  output Modelica.Units.SI.Voltage der_Psi "Actual derivative of flux";
protected
  Modelica.Units.SI.Permeance G_mnom=saturationData.G_mnom;
  Modelica.Units.SI.Current V_mnom=saturationData.V_mnom;
  Modelica.Units.SI.Current V_mpar=saturationData.V_mpar;
  Modelica.Units.SI.Permeance G_mzer=saturationData.G_mzer;
  Modelica.Units.SI.Permeance G_minf=saturationData.G_minf;
algorithm
  der_Psi :=if saturationData.useSaturation then
    (G_minf + (G_mzer - G_minf)/((V_m/V_mpar)^2 + 1))*der_V_m
    else 0;
  annotation (Documentation(info="<html>
<p>
Calculates derivative of saturated flux:
</p>
<p>
<code>der_psi = dpsi/di*der_V_m</code>
</p>
</html>"));
end derSaturatedFlux;
