within Magnetic_DCMachines.Saturation;
package Functions
  extends Modelica.Icons.FunctionsPackage;





  function saturatedPermeance "Caluclates permeance dependent on magnetic potential difference"
    extends Modelica.Icons.Function;
    import Modelica.Constants.small;
    import Modelica.Math.atan;
    input ParameterRecords.SaturationData saturationData "Parameter record for saturation";
  input Modelica.Units.SI.Current V_m "Actual magnetic potential difference";
  output Modelica.Units.SI.Permeance G_mact "Actual permeance";
protected
  Modelica.Units.SI.Permeance G_mnom=saturationData.G_mnom;
  Modelica.Units.SI.Current V_mnom=saturationData.V_mnom;
  Modelica.Units.SI.Current V_mpar=saturationData.V_mpar;
  Modelica.Units.SI.Permeance G_mzer=saturationData.G_mzer;
  Modelica.Units.SI.Permeance G_minf=saturationData.G_minf;
  algorithm
    G_mact :=if saturationData.useSaturation then G_minf +
      (G_mzer - G_minf)*(if abs(V_m)/V_mpar <small then 1 else atan(V_m/V_mpar)/(V_m/V_mpar))
      else G_mnom;
    annotation (derivative=derSaturatedPermeance,
      Documentation(info="<html>
<p>
Calculates the saturated permeance <code>G_mact/code> dependent on current <code>V_m</code>:
</p>
<p>
<code>G_mact = G_minf + (G_mzer - G_minf)*atan(V_m/Ipar)/(V_m/Ipar)</code>
</p>
</html>"));
  end saturatedPermeance;

  function derSaturatedPermeance
    "Caluclates derivative of permeance dependent on current"
    import Modelica.Constants.small;
    import Modelica.Math.atan;
    input ParameterRecords.SaturationData saturationData "Parameter record for saturation";
  input Modelica.Units.SI.Current V_m "Actual current";
  input Modelica.Units.SI.CurrentSlope der_V_m "Actual derivative of current";
    output Types.PermeanceSlope derG_m "Actual permeance slope";
protected
  Modelica.Units.SI.Permeance G_mnom=saturationData.G_mnom;
  Modelica.Units.SI.Current V_mnom=saturationData.V_mnom;
  Modelica.Units.SI.Current V_mpar=saturationData.V_mpar;
  Modelica.Units.SI.Permeance G_mzer=saturationData.G_mzer;
  Modelica.Units.SI.Permeance G_minf=saturationData.G_minf;
  algorithm
    derG_m := if saturationData.useSaturation then (G_mzer - G_minf)*
      (if abs(V_m)/V_mpar < small then 0 else
      (1/((V_m/V_mpar)^2 + 1) - atan(V_m/V_mpar)/(V_m/V_mpar))/V_m*der_V_m)
      else 0;
    annotation (Documentation(info="<html>
<p>
Calculates derivative of saturated permeance:
</p>
<p>
<code>der_L = dL/di*der_V_m</code>
</p>
</html>"));
  end derSaturatedPermeance;

  annotation (Documentation(info="<html>
<p>
Functions used for saturation
</p>
</html>"));
end Functions;
