within Magnetic_DCMachines.Saturation.Functions;
function iterateParameter "Iterate current parameter"
  extends Modelica.Icons.Function;
  import Modelica.Constants.eps;
  import Modelica.Math.Nonlinear.solveOneNonlinearEquation;
  input Modelica.Units.SI.Permeance G_mnom
    "Nominal permeance at Nominal current";
  input Modelica.Units.SI.Current V_mnom "Nominal current";
  input Modelica.Units.SI.Permeance G_mzer "Permeance near current=0";
  input Modelica.Units.SI.Permeance G_minf "Permeance at large currents";
  output Modelica.Units.SI.Current V_mpar "Current parameter";
algorithm
  assert(G_mzer > G_mnom*(1 + eps), "G_mzer has to be > G_mnom");
  assert(G_minf  < G_mnom*(1 - eps), "G_minf  has to be < G_mnom");
  assert(G_minf  > eps, "G_minf  has to be > 0");
  V_mpar := V_mnom*solveOneNonlinearEquation(
    function scaledParameter(gzer=G_mzer/G_mnom, ginf=G_minf/G_mnom),
    1e-6, 1e6);
  annotation (Documentation(info="<html>
<p>This function iterates the current parameter <code>V_mpar</code> to fulfill the equation: </p>
<p><code>G_mnom*V_mnom = G_minf*V_mnom +  (G_mzer - Linf)*V_mpar*atan(V_mnom/Ipar)</code></p>
</html>"));
end iterateParameter;
