within Magnetic_DCMachines.Saturation.Functions;
function scaledParameter
  "Function for iteration of scaled current parameter"
  extends Modelica.Math.Nonlinear.Interfaces.partialScalarFunction;
  import Modelica.Constants.pi;
  import Modelica.Math.atan;
  input Real gzer "Scaled permeance near current=0";
  input Real ginf "Scaled permeance at large currents";
algorithm
  y := (1 - ginf)/(gzer - ginf) - u*(pi/2  - atan(u));
  annotation (Documentation(info="<html>
<p>Function internally used for iteration of the scaled parameter <code>y = V_mpar/V_mnom</code> to fulfll the equation</p>
<p><code>(G_mnom - G_minf)/(G_mzer - Linf)*V_mnom/Ipar = atan(V_mnom/Ipar)</code></p>
</html>"));
end scaledParameter;
