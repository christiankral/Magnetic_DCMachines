within Magnetic_DCMachines.Saturation.Functions;
function saturatedFlux "Calculates flux dependent on current"
  extends Modelica.Icons.Function;
  import Modelica.Math.atan;
  input ParameterRecords.SaturationData saturationData "Parameter record for saturation";
  input Modelica.Units.SI.Current V_m "Actual current";
  output Modelica.Units.SI.MagneticFlux Psi "Actual flux";
protected
  Modelica.Units.SI.Permeance G_mnom=saturationData.G_mnom;
  Modelica.Units.SI.Current V_mnom=saturationData.V_mnom;
  Modelica.Units.SI.Current V_mpar=saturationData.V_mpar;
  Modelica.Units.SI.Permeance G_mzer=saturationData.G_mzer;
  Modelica.Units.SI.Permeance G_minf=saturationData.G_minf;
algorithm
  Psi :=if saturationData.useSaturation then G_minf*V_m + (G_mzer - G_minf)*V_mpar*atan(V_m/V_mpar)
    else G_mnom*V_m;
  annotation (derivative=derSaturatedFlux,
    Documentation(info="<html>
<p>
Calculates the saturated flux <code>psi</code> dependent on current <code>V_m</code>:
</p>
<p>
<code>psi = G_minf*V_m + (G_mzer - G_minf)*V_mpar*atan(V_m/Ipar)</code>
</p>
</html>"));
end saturatedFlux;
