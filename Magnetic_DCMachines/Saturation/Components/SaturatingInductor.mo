within Magnetic_DCMachines.Saturation.Components;
model SaturatingInductor "Simple model of an inductor with saturation"
  extends Modelica.Magnetic.FluxTubes.Interfaces.TwoPort;
  import Magnetic_DCMachines.Saturation.Functions.saturatedFlux;
  parameter ParameterRecords.SaturationData saturationData "Parameters of saturated inductance"
    annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
protected
  final parameter Boolean useSaturation=saturationData.useSaturation;
equation
  Phi = saturatedFlux(saturationData, V_m);
  annotation (defaultComponentName="inductor",
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
            100}}), graphics={
        Text(
          extent={{-150,90},{150,50}},
          textString="%name",
          textColor={0,0,255}),
      Rectangle(
        extent={{-70,30},{70,-30}},
        lineColor={255,128,0},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid),
      Line(points={{-70,0},{-90,0}}, color={255,128,0}),
      Line(points={{70,0},{90,0}}, color={255,128,0}),
        Line(
          points={{80,50},{40,40},{0,0},{-40,-40},{-80,-52}},
          color={255,128,0},
          smooth=Smooth.Bezier)}),
    Documentation(info="<html>
<p>
This model approximates the behaviour of an inductor under the influence of saturation.
The value of the inductance depends on the current flowing through the inductor, based on an approximation based on the atan-function.
The inductance decreases as current increases.
</p>
<p>
For special usage in quasiStatic DC machines, the voltage drop <code>v</code> is set to <code>0</code>, if <code>constant Boolean quasiStatic = true</code> (default is <code>quasiStatic = false</code>).
</p>
</html>",
        revisions="<html>
<dl>
  <dt><strong>Main Author:</strong></dt>
  <dd>
  <a href=\"https://www.haumer.at/\">Anton Haumer</a><br>
  Technical Consulting &amp; Electrical Engineering<br>
  D-93049 Regensburg<br>Germany<br>
  email: <a href=\"mailto:a.haumer@haumer.at\">a.haumer@haumer.at</a>
  </dd>
  <dt><strong>Release Notes:</strong></dt>
  <dd>2019-07-19: Improved iteration of parameter Ipar</dd>
  <dd>2004-03-27: 1st implementation</dd>
 </dl>
</html>"));
end SaturatingInductor;
