within Magnetic_DCMachines.Saturation;
package Components "Components modeled with saturation"
  extends Modelica.Icons.Package;

  annotation (Icon(graphics={
        Line(points={{-90,0},{-60,0}}),
        Line(
          points={{-60,0},{-59,6},{-52,14},{-38,14},{-31,6},{-30,0}},
          smooth=Smooth.Bezier),
        Line(
          points={{-30,0},{-29,6},{-22,14},{-8,14},{-1,6},{0,0}},
          smooth=Smooth.Bezier),
        Line(
          points={{0,0},{1,6},{8,14},{22,14},{29,6},{30,0}},
          smooth=Smooth.Bezier),
        Line(
          points={{30,0},{31,6},{38,14},{52,14},{59,6},{60,0}},
          smooth=Smooth.Bezier),
        Line(points={{60,0},{90,0}}),
        Rectangle(
          extent={{-60,-10},{60,-20}},
          fillPattern=FillPattern.Sphere,
          fillColor={128,128,128})}), Documentation(info="<html>
<p>
Compnents that are modeled with saturation.
</p>
</html>"));
end Components;
