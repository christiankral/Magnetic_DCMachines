within Magnetic_DCMachines;
package Saturation "Approximation of saturaton based on atan-function"
  extends Modelica.Icons.Package;

  annotation (
  Documentation(info="<html>
<p>
Saturation models based on approximation using the atan-function
</p>
</html>"),
         Icon(graphics={                   Line(points={{-80,-80},{-52.7,-75.2},
          {-37.4,-69.7},{-26.9,-63},{-19.7,-55.2},{-14.1,-45.8},{-10.1,-36.4},
          {-6.03,-23.9},{-1.21,-5.06},{5.23,21},{9.25,34.1},{13.3,44.2},{18.1,
          52.9},{24.5,60.8},{33.4,67.6},{47,73.6},{69.5,78.6},{80,80}}),
                                                               Line(
          points={{0,-90},{0,84}}, color={192,192,192}),
                            Polygon(
            points={{0,100},{-6,84},{6,84},{0,100}},
            lineColor={192,192,192},
            fillColor={192,192,192},
            fillPattern=FillPattern.Solid),
                               Polygon(
            points={{100,0},{84,6},{84,-6},{100,0}},
            lineColor={192,192,192},
            fillColor={192,192,192},
            fillPattern=FillPattern.Solid),Line(points={{-100,0},{84,0}},
          color={192,192,192})}));
end Saturation;
