within ;
package Magnetic_DCMachines "DC machines based on magnetic circutis"
extends Modelica.Icons.Package;

  annotation (
    version="1",
    versionDate="2022-XX-XX",
    uses(Modelica(version="4.0.0"), Complex(version="4.0.0")),
        Icon(graphics={Rectangle(
        extent={{-20,76},{20,-76}},
        lineColor={255,128,0},
        fillColor={255,128,0},
        fillPattern=FillPattern.Solid), Ellipse(
        extent={{-60,60},{60,-60}},
        lineColor={255,128,0},
        fillColor={255,255,255},
        fillPattern=FillPattern.Solid)}));
end Magnetic_DCMachines;
