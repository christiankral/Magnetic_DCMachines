within Magnetic_DCMachines.BasicMachines.Components;
model SinglePhaseWinding "Symmetric winding model coupling electrical and magnetic domain"

  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p "Positive pin"
    annotation (Placement(transformation(
        origin={-100,100},
        extent={{-10,-10},{10,10}},
        rotation=180)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_n "Negative pin"
    annotation (Placement(transformation(
        origin={-100,-100},
        extent={{-10,-10},{10,10}},
        rotation=180)));
  Modelica.Magnetic.FundamentalWave.Interfaces.PositiveMagneticPort
    port_p "Positive magnetic port" annotation (Placement(
        transformation(extent={{90,90},{110,110}})));
  Modelica.Magnetic.FundamentalWave.Interfaces.NegativeMagneticPort
    port_n "Negative magnetic port" annotation (Placement(
        transformation(extent={{90,-110},{110,-90}})));
  parameter Boolean useHeatPort=false
    "Enable / disable (=fixed temperatures) thermal port"
    annotation (Evaluate=true);
  parameter Modelica.Units.SI.Resistance RRef(start=1)
    "Winding resistance per phase at TRef";
  parameter Modelica.Units.SI.Temperature TRef(start=293.15)
    "Reference temperature of winding";
  parameter
    Modelica.Electrical.Machines.Thermal.LinearTemperatureCoefficient20
    alpha20(start=0) "Temperature coefficient of winding at 20 degC";
  final parameter Modelica.Units.SI.LinearTemperatureCoefficient alphaRef=
      Modelica.Electrical.Machines.Thermal.convertAlpha(
      alpha20,
      TRef,
      293.15);
  parameter Modelica.Units.SI.Temperature TOperational(start=293.15)
    "Operational temperature of winding"
    annotation (Dialog(enable=not useHeatPort));
  parameter Modelica.Units.SI.Inductance Lsigma(start=1)
    "Winding stray inductance per phase";
  parameter Real effectiveTurns = 1 "Effective number of turns per phase";
  parameter Modelica.Units.SI.Angle orientation=0 "Orientation of winding axis";

  Modelica.Units.SI.Voltage v=pin_p.v - pin_n.v "Voltage";
  Modelica.Units.SI.Current i=pin_p.i "Current";

  Modelica.Units.SI.ComplexMagneticPotentialDifference V_m=port_p.V_m - port_n.V_m
    "Magnetic potential difference";
  Modelica.Units.SI.ComplexMagneticFlux Phi=port_p.Phi "Magnetic flux";

  Modelica.Electrical.Analog.Basic.Resistor resistor(
    final useHeatPort=useHeatPort,
    final R=RRef,
    final T_ref=TRef,
    final alpha=alphaRef,
    final T=TOperational) "Winding resistor"
        annotation (Placement(transformation(
        origin={-20,70},
        extent={{10,10},{-10,-10}},
        rotation=90)));
  Magnetic_DCMachines.Components.ElectroMagneticConverter electroMagneticConverter(final effectiveTurns=effectiveTurns, final orientation=orientation) "Winding" annotation (Placement(transformation(extent={{-8,-10},{12,10}})));
  Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a heatPortWinding
 if useHeatPort "Heat ports of winding resistor"
    annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));
  Magnetic_DCMachines.Components.Permeance permeance(G_m=Lsigma/effectiveTurns^2) "Stray permeance"
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,0})));
equation
  connect(pin_p, resistor.p) annotation (Line(points={{-100,100},{-20,100},{-20,80}}, color={0,0,255}));
  connect(electroMagneticConverter.port_p, port_p) annotation (Line(points={{12,10},{12,100},{100,100}}, color={255,128,0}));
  connect(electroMagneticConverter.port_n, port_n) annotation (Line(points={{12,-10},{12,-100},{100,-100}}, color={255,128,0}));
  connect(heatPortWinding, resistor.heatPort) annotation (Line(points={{0,-100},{0,-60},{-40,-60},{-40,70},{-30,70}}, color={191,0,0}));
  connect(electroMagneticConverter.port_p, permeance.port_p) annotation (Line(points={{12,10},{12,100},{30,100},{30,10}}, color={255,128,0}));
  connect(electroMagneticConverter.port_n, permeance.port_n) annotation (Line(points={{12,-10},{12,-100},{30,-100},{30,-10}}, color={255,128,0}));
  connect(resistor.n, electroMagneticConverter.pin_p) annotation (Line(points={{-20,60},{-20,10},{-8,10}}, color={0,0,255}));
  connect(electroMagneticConverter.pin_n, pin_n) annotation (Line(points={{-8,-10},{-20,-10},{-20,-100},{-100,-100}}, color={0,0,255}));
  annotation (defaultComponentName="winding",
      Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={      Line(points={{100,-100},{
          94,-100},{84,-98},{76,-94},{64,-86},{50,-72},{42,-58},{36,-40},
          {30,-18},{30,0},{30,18},{34,36},{46,66},{62,84},{78,96},{90,100},
          {100,100}}, color={255,128,0}),Line(points={{40,60},{-100,60},{
          -100,100}}, color={0,0,255}),Line(points={{40,-60},{-100,-60},{
          -100,-98}}, color={0,0,255}),Line(points={{40,60},{100,20},{40,
          -20},{0,-20},{-40,0},{0,20},{40,20},{100,-20},{40,-60}}, color=
          {0,0,255}),
          Text(
              extent={{-150,110},{150,150}},
              lineColor={0,0,255},
              textString="%name")}),   Documentation(info="<html>
<p>
The single phase winding consists of a winding
<a href=\"modelica://Modelica.Electrical.Analog.Basic.Resistor\">resistor</a>, a
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.Components.SinglePhaseElectroMagneticConverter\">single phase electromagnetic coupling</a> and a <a href=\"modelica://Modelica.Magnetic.FundamentalWave.Components.Reluctance\">stray reluctance</a>.
</p>

<h4>See also</h4>
<p>
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SymmetricMultiPhaseWinding\">SymmetricMultiPhaseWinding</a>,
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SymmetricMultiPhaseCageWinding\">SymmetricMultiPhaseCageWinding</a>,
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SaliencyCageWinding\">SaliencyCageWinding</a>
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.RotorSaliencyAirGap\">RotorSaliencyAirGap</a>
</p>
</html>"));
end SinglePhaseWinding;
