within Magnetic_DCMachines.BasicMachines.Components;
model AirGap "Air gap model with armature saliency"
  import Modelica.Constants.pi;
  import Modelica.ComplexMath.real;
  import Modelica.ComplexMath.imag;
  import Magnetic_DCMachines.Saturation.Functions.saturatedPermeance;
  Modelica.Magnetic.FundamentalWave.Interfaces.PositiveMagneticPort port_ep "Positive complex magnetic excitation port" annotation (Placement(transformation(extent={{-110,-110},{-90,-90}})));
  Modelica.Magnetic.FundamentalWave.Interfaces.NegativeMagneticPort port_en "Negative complex magnetic excitation port" annotation (Placement(transformation(extent={{-110,90},{-90,110}})));
  Modelica.Magnetic.FundamentalWave.Interfaces.PositiveMagneticPort port_ap "Positive complex magnetic armature port" annotation (Placement(transformation(extent={{90,90},{110,110}})));
  Modelica.Magnetic.FundamentalWave.Interfaces.NegativeMagneticPort port_an "Negative complex magnetic armature port" annotation (Placement(transformation(extent={{90,-110},{110,-90}})));
  Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_a  "Flange of the armature" annotation (Placement(transformation(extent={{-10,110},{10,90}})));
  Modelica.Mechanics.Rotational.Interfaces.Flange_a support "Support at which the reaction torque is acting" annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));

  parameter Integer p "Number of pole pairs";
  parameter Modelica.Units.SI.Permeance G_ma(start=1)
    "Permeance of excitation magnetic circuit" annotation (Evaluate=true);
  parameter Boolean useSaturation = false "Use saturation, if true";
  parameter Modelica.Units.SI.Permeance G_me(start=1)
    "Permeance of armature magnetic circuit at V_meNom"
    annotation (Evaluate=true);
  parameter Modelica.Units.SI.Permeance G_mezer=G_me*5
    "Permeance of armature magnetic circuit at zero magnetic potential difference"
    annotation (Dialog(enable=useSaturation));
  parameter Modelica.Units.SI.Permeance G_meinf=G_me/5
    "Permeance of armature magnetic circuit at infinity magnetic potential difference"
    annotation (Dialog(enable=useSaturation));
  parameter Modelica.Units.SI.MagneticPotentialDifference V_menom(start=1)
    "Magnetic potential difference w.r.t. G_me"
    annotation (Dialog(enable=useSaturation));

  // Complex phasors of magnetic potential differences
  Modelica.Units.SI.ComplexMagneticPotentialDifference V_mee
    "Complex magnetic potential difference of excitation w.r.t. excitation fixed frame";
  Modelica.Units.SI.ComplexMagneticPotentialDifference V_mme
    "Complex magnetic magnetizing potential";
  Modelica.Units.SI.ComplexMagneticPotentialDifference V_maa
    "Complex magnetic potential difference of armature w.r.t. armature fixed frame";
  Modelica.Units.SI.ComplexMagneticPotentialDifference V_mae
    "Complex magnetic potential difference of armature w.r.t. excitation fixed frame";
  // Complex phasors of magnetic fluxes
  Modelica.Units.SI.ComplexMagneticFlux Phi_ee
    "Complex magnetic flux of excitation w.r.t. excitation fixed frame";
  Modelica.Units.SI.ComplexMagneticFlux Phi_aa
    "Complex magnetic flux of armature w.r.t. armature fixed frame";
  Modelica.Units.SI.ComplexMagneticFlux Phi_ae
    "Complex magnetic flux of armature w.r.t. excitation fixed frame";
  // Electrical torque and mechanical angle
  Modelica.Units.SI.Torque tauElectrical "Electrical torque";
  Modelica.Units.SI.Angle gamma
    "Electrical angle between armature and excitation";
  Modelica.Units.SI.AngularVelocity w "Mechanical angular velocity";
  Complex rotator "Equivalent vector representation of orientation";
  parameter Saturation.ParameterRecords.SaturationData saturationData(
    final G_mnom=G_me,
    final useSaturation=useSaturation,
    final V_mnom=V_menom,
    final G_mzer=G_mezer,
    final G_minf=G_meinf) annotation (Placement(transformation(extent={{-10,-8},{10,12}})));
equation
  port_ep.Phi = Phi_ee "excitation flux into positive excitation port";
  port_ep.Phi +port_en.Phi  = Complex(0, 0) "Balance of excitation flux";
  port_ap.Phi = Phi_aa "armature flux into positive armature port";
  port_ap.Phi +port_an.Phi  = Complex(0, 0) "Balance of armature flux";
  port_ep.V_m -port_en.V_m  = V_mee "Magnetomotive force of excitation (excitation fixed)";
  port_ap.V_m -port_an.V_m  = V_maa "Magnetomotive force of armature (armature fixed)";
  // Transformations
  V_mae = V_maa * rotator;
  Phi_ae = Phi_aa * rotator;
  // excitation flux and armature flux are equal
  Phi_ee = Phi_ae;
  // Magnetizing potential difference
  Phi_ee.re = G_ma * (V_mee.re - V_mme.re);
  Phi_ee.im = saturatedPermeance(saturationData, V_mee.im - V_mme.im) * (V_mee.im - V_mme.im);
  // Local balance of armature and magnetizing magnetic potential difference
  V_mme + V_mae = Complex(0, 0);
  // Torque
  tauElectrical = -p * (Phi_ee.im * V_mme.re - Phi_ee.re * V_mme.im);
  flange_a.tau = -tauElectrical;
  support.tau = tauElectrical;
  // Electrical angle between excitation and armature
  gamma = p*(flange_a.phi - support.phi);
  w = der(flange_a.phi) - der(support.phi);
  rotator = Modelica.ComplexMath.exp(Complex(0, gamma));
    annotation(defaultComponentName="airGap", Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{100,100}}), graphics={
        Ellipse(
          extent={{-100,100},{100,-100}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(points={{-100,90},{-100,60},{-80,60}}, color={255,128,0}),
        Line(points={{-100,-90},{-100,-60},{-80,-60}}, color={255,128,0}),
        Line(points={{40,60},{100,60},{100,90}}, color={255,128,0}),
        Line(points={{40,-60},{100,-60},{100,-90}}, color={255,128,0}),
        Ellipse(
          extent={{-80,80},{80,-80}},
          fillColor={255,255,255},
          fillPattern=FillPattern.Solid),
        Line(points={{0,80},{0,90}})}), Documentation(info="<html>
<p>
This salient air gap model can be used for machines with uniform air gaps and for machines with armature saliency. The air gap model is not symmetrical towards excitation and armature since it is assumed the saliency always refers to the armature. The saliency of the air gap is represented by a main field inductance in the d- and q-axis.
</p>

<p>
For the mechanical interaction of the air gap model with the excitation and the armature is equipped with two
<a href=\"modelica://Modelica.Mechanics.Rotational.Interfaces.Flange_a\">rotational connectors</a>. The torques acting on both connectors have the same absolute values but different signs. The difference between the excitation and the armature angle,
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/gamma.png\">, is required for the transformation of the magnetic excitation quantities to the armature side.</p>

<p>
The air gap model has two magnetic excitation and two magnetic armature
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.Interfaces.MagneticPort\">ports</a>. The magnetic potential difference and the magnetic flux of the excitation (superscript s) are transformed to the armature fixed reference frame (superscript r). The effective reluctances of the main field with respect to the d- and q-axis are considered then in the balance equations
</p>

<p>
&nbsp;&nbsp;<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Machines/Components/airgap.png\">
</p>

<p>
according to the following figure.
</p>
<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">
  <caption align=\"bottom\"><strong>Fig:</strong> Magnetic equivalent circuit of the air gap model</caption>
  <tr>
    <td>
      <img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Machines/Components/airgap_phasors.png\">
    </td>
  </tr>
</table>

<h4>See also</h4>
<p>
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SinglePhaseWinding\">SinglePhaseWinding</a>,
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SymmetricMultiPhaseWinding\">SymmetricMultiPhaseWinding</a>,
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SymmetricMultiPhaseCageWinding\">SymmetricMultiPhaseCageWinding</a>
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.BasicMachines.Components.SaliencyCageWinding\">SaliencyCageWinding</a>
</p>

</html>"));
end AirGap;
