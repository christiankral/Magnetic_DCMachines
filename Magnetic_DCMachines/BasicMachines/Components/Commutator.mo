within Magnetic_DCMachines.BasicMachines.Components;
model Commutator "Ideal commutator for DC machines"
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p "Positive pin" annotation (Placement(transformation(origin={-100,100},extent={{-10,-10},{10,10}},rotation=180)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_n "Negative pin" annotation (Placement(transformation(origin={-100,-100},extent={{-10,-10},{10,10}},rotation=180)));
  Modelica.Magnetic.FundamentalWave.Interfaces.PositiveMagneticPort port_p "Positive complex magnetic port" annotation (Placement(transformation(extent={{90,90},{110,110}})));
  Modelica.Magnetic.FundamentalWave.Interfaces.NegativeMagneticPort port_n "Negative complex magnetic port" annotation (Placement(transformation(extent={{90,-110},{110,-90}})));
  Modelica.Mechanics.Rotational.Interfaces.Flange_a flange "Flange" annotation (Placement(transformation(extent={{-10,110},{10,90}})));
  Modelica.Mechanics.Rotational.Interfaces.Flange_a support "Support" annotation (Placement(transformation(extent={{-10,-110},{10,-90}})));

  parameter Integer p = 1 "Number of pole pairs";
  parameter Real effectiveTurns = 1 "Effective number of turns" annotation (Evaluate=true);
  parameter Modelica.Units.SI.Angle orientation=0
    "Orientation of the resulting fundamental wave V_m phasor"
    annotation (Evaluate=true);

  // Local electric single phase quantities
  Modelica.Units.SI.Voltage v "Voltage";
  Modelica.Units.SI.Current i "Current";

  // Local electromagnetic quantities
  Modelica.Units.SI.ComplexMagneticPotentialDifference V_m
    "Complex magnetic potential difference";
  Modelica.Units.SI.MagneticPotentialDifference abs_V_m=
      Modelica.ComplexMath.abs(V_m)
    "Magnitude of complex magnetic potential difference";
  Modelica.Units.SI.Angle arg_V_m=Modelica.ComplexMath.arg(V_m)
    "Argument of complex magnetic potential difference";

  Modelica.Units.SI.ComplexMagneticFlux Phi "Complex magnetic flux";
  Modelica.Units.SI.MagneticFlux abs_Phi=Modelica.ComplexMath.abs(Phi)
    "Magnitude of complex magnetic flux";
  Modelica.Units.SI.Angle arg_Phi=Modelica.ComplexMath.arg(Phi)
    "Argument of complex magnetic flux";

  Modelica.Units.SI.Torque tau "Torque";
  Modelica.Units.SI.Angle phi "Mechanical angle";
  Modelica.Units.SI.Angle gamma "Electrical angle";
  Modelica.Units.SI.AngularVelocity w "Angular velocity";

equation
  // Mechanical torque = 0, angle phi = relative angle of flange and support
  tau = 0;
  flange.tau = tau;
  support.tau = tau;
  phi = flange.phi - support.phi;
  gamma = p * phi;
  w = der(phi);
  // Magnetic flux and flux balance of the magnetic ports
  port_p.Phi = Phi;
  port_p.Phi + port_n.Phi = Complex(0, 0);
  // Magnetic potential difference of the magnetic ports
  port_p.V_m - port_n.V_m = V_m;
  // Voltage drop between the electrical pins
  v = pin_p.v - pin_n.v;
  // Current and current balance of the electric pins
  i = pin_p.i;
  pin_p.i + pin_n.i = 0;
  // Complex magnetic potential difference is determined from currents, number
  // of turns and angles of orientation of winding relative to stator;
  V_m = effectiveTurns * i * Complex(cos(gamma + orientation), -sin(gamma + orientation));
  // Induced voltages is determined from complex magnetic flux, number of turns
  // and angles of orientation of winding
  -v = effectiveTurns * (cos(gamma + orientation) * der(Phi.re) - sin(gamma + orientation) * der(Phi.im));
  annotation (
    Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},{
            100,100}}), graphics={
                           Line(points={{0,-60},{-100,-60},{-100,-100}},
          color={0,0,255}),
          Text(
              extent={{-150,120},{150,160}},
              lineColor={0,0,255},
              textString="%name"),
        Polygon(
          points={{30,18},{34,36},{44,60},{0,60},{0,-60},{44,-60},{34,-36},{30,-18},{30,18}},
          lineColor={0,0,255},
          fillColor={0,0,255},
          fillPattern=FillPattern.Solid),Line(points={{40,60},{-100,60},{
          -100,100}}, color={0,0,255}),        Line(points={{100,-100},{94,-100},{84,-98},{76,-94},{62,-84},{44,-60},{34,-36},{30,-18},{30,0},{30,18},{34,36},{44,60},{62,84},{78,96},{90,100},{100,100}},
                      color={255,128,0})}),
    Documentation(info="<html>
<p>
The single phase winding has an effective number of turns, <img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/effectiveTurns.png\"> and a respective orientation of the winding, <img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/orientation.png\">. The current in winding is <img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/i.png\">.
</p>

<p>
The total complex magnetic potential difference of the single phase winding is determined by:
</p>

<p>
&nbsp;&nbsp;<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Components/singlephaseconverter_vm.png\">
</p>

<p>
In this equation the magnetomotive force is aligned with the orientation of the winding.
</p>

<p>
The voltage <img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/v.png\"> induced in the winding depends on the cosine between the orientation of the winding and the angle of the complex magnetic flux. Additionally, the magnitudes of the induced voltages are proportional to the respective number of turns. This relationship can be modeled by means of</p>

<p>
&nbsp;&nbsp;<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Components/singlephaseconverter_phi.png\">
</p>

<p>The single phase electromagnetic converter is a special case of the
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.Components.MultiPhaseElectroMagneticConverter\">MultiPhaseElectroMagneticConverter</a>
</p>

<h4>See also</h4>
<p>
<a href=\"modelica://Modelica.Magnetic.FundamentalWave.Components.MultiPhaseElectroMagneticConverter\">MultiPhaseElectroMagneticConverter</a>
</p>

</html>"));
end Commutator;
