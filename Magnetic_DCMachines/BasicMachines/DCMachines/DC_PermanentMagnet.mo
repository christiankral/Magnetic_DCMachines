within Magnetic_DCMachines.BasicMachines.DCMachines;
model DC_PermanentMagnet "Permanent magnet DC machine"
  extends Magnetic_DCMachines.BaseClasses.DCMachine(
    final ViNominal=VaNominal - Modelica.Electrical.Machines.Thermal.convertResistance(
        Ra,
        TaRef,
        alpha20a,
        TaNominal)*IaNominal - Modelica.Electrical.Machines.Losses.DCMachines.brushVoltageDrop(brushParameters, IaNominal),
    final PhiNominal=Phi,
    redeclare final Modelica.Electrical.Machines.Thermal.DCMachines.ThermalAmbientDCPM thermalAmbient(final Tpm=TpmOperational),
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.ThermalPortDCPM thermalPort,
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.ThermalPortDCPM internalThermalPort,
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.PowerBalanceDCPM powerBalance(final lossPowerPermanentMagnet=0),
    airGap(final useSaturation=false,
      final G_me=1,
      final V_menom=1));
  final parameter Modelica.Units.SI.Temperature TpmOperational=293.15
    "Operational temperature of permanent magnet"
    annotation (Dialog(group="Operational temperatures"));
  Modelica.Magnetic.FundamentalWave.Sources.ConstantFlux magFluxSource(final Phi = Complex(0, Phi)) annotation (Placement(transformation(extent={{0,-60},{-20,-40}})));
protected
  constant Modelica.Units.SI.MagneticFlux Phi=1 "Field excitation flux";
equation
  connect(magFluxSource.port_n,airGap.port_en)  annotation (Line(points={{-20,-50},{-30,-50},{-30,-30},{-20,-30}}, color={255,128,0}));
  connect(airGap.port_ep, magFluxSource.port_p) annotation (Line(points={{0,-30},{10,-30},{10,-50},{0,-50}}, color={255,128,0}));
  annotation (
    defaultComponentName="dcpm",
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{
            100,100}}), graphics={Rectangle(
          extent={{-130,10},{-100,-10}},
          fillColor={0,255,0},
          fillPattern=FillPattern.Solid), Rectangle(
          extent={{-100,10},{-70,-10}},
          fillColor={255,0,0},
          fillPattern=FillPattern.Solid)}),
    Documentation(info="<html>
<p><strong>Model of a DC Machine with permanent magnets.</strong><br>
Armature resistance and inductance are modeled directly after the armature pins, then using a <em>AirGapDC</em> model. Permanent magnet excitation is modelled by a constant equivalent excitation current feeding AirGapDC. The machine models take the following loss effects into account:
</p>

<ul>
<li>heat losses in the temperature dependent armature winding resistance</li>
<li>brush losses in the armature circuit</li>
<li>friction losses</li>
<li>core losses (only eddy current losses, no hysteresis losses)</li>
<li>stray load losses</li>
</ul>

<p>No saturation is modeled, since permanent magnet excitation is assumed to be constant.
<br><strong>Default values for machine's parameters (a realistic example) are:</strong><br></p>
<table>
<tr>
<td>stator's moment of inertia</td>
<td>0.29</td><td>kg.m2</td>
</tr>
<tr>
<td>rotor's moment of inertia</td>
<td>0.15</td><td>kg.m2</td>
</tr>
<tr>
<td>nominal armature voltage</td>
<td>100</td><td>V</td>
</tr>
<tr>
<td>nominal armature current</td>
<td>100</td><td>A</td>
</tr>
<tr>
<td>nominal speed</td>
<td>1425</td><td>rpm</td>
</tr>
<tr>
<td>nominal torque</td>
<td>63.66</td><td>Nm</td>
</tr>
<tr>
<td>nominal mechanical output</td>
<td>9.5</td><td>kW</td>
</tr>
<tr>
<td>efficiency</td>
<td>95.0</td><td>%</td>
</tr>
<tr>
<td>armature resistance</td>
<td>0.05</td><td>Ohm at reference temperature</td>
</tr>
<tr>
<td>reference temperature TaRef</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>temperature coefficient alpha20a </td>
<td>0</td><td>1/K</td>
</tr>
<tr>
<td>armature inductance</td>
<td>0.0015</td><td>H</td>
</tr>
<tr>
<td>armature nominal temperature TaNominal</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>armature operational temperature TaOperational</td>
<td>20</td><td>&deg;C</td>
</tr>
</table>
Armature resistance resp. inductance include resistance resp. inductance of commutating pole winding and compensation winding, if present.
</html>"));
end DC_PermanentMagnet;
