within Magnetic_DCMachines.BasicMachines.DCMachines;
model DC_ElectricalExcited
  "Electrical shunt/separate excited linear DC machine"
  import Modelica.Constants.pi;
  extends Magnetic_DCMachines.BaseClasses.DCMachine(
    final ViNominal=VaNominal - Modelica.Electrical.Machines.Thermal.convertResistance(
        Ra,
        TaRef,
        alpha20a,
        TaNominal)*IaNominal - Modelica.Electrical.Machines.Losses.DCMachines.brushVoltageDrop(brushParameters, IaNominal),
    final PhiNominal=Lme*IeNominal/Ne,
    redeclare final Modelica.Electrical.Machines.Thermal.DCMachines.ThermalAmbientDCEE thermalAmbient(final Te=TeOperational),
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.ThermalPortDCEE thermalPort,
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.ThermalPortDCEE internalThermalPort,
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.PowerBalanceDCEE powerBalance(final powerExcitation=ve*ie, final lossPowerExcitation=excitationWinding.resistor.LossPower),
    airGap(final useSaturation=useSaturation,
      final G_me=Lme/Ne^2,
      final G_mezer=Lmezer/Ne^2,
      final G_meinf=Lmeinf/Ne^2,
      final V_menom=Ne*IeNominal));
  parameter Modelica.Units.SI.Current IeNominal(start=1)
    "Nominal excitation current" annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Resistance Re(start=100)
    "Field excitation resistance at TeRef" annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Temperature TeRef(start=293.15)
    "Reference temperature of excitation resistance"
    annotation (Dialog(tab="Excitation"));
  parameter
    Modelica.Electrical.Machines.Thermal.LinearTemperatureCoefficient20
    alpha20e(start=0)
    "Temperature coefficient of excitation resistance"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Inductance Le(start=1)
    "Total field excitation inductance" annotation (Dialog(tab="Excitation"));
  parameter Real sigmae(
    final min=0,
    final max=0.99,
    start=0) "Stray fraction of total excitation inductance"
    annotation (Dialog(tab="Excitation"));
  parameter Boolean useSaturation=false "Take saturation into account?"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Inductance Lezer=5*Le
    "Inductance at small current"
    annotation (Dialog(tab="Excitation", enable=useSaturation));
  parameter Modelica.Units.SI.Inductance Leinf=Le/5
    "Inductance at large current"
    annotation (Dialog(tab="Excitation", enable=useSaturation));
  parameter Modelica.Units.SI.Temperature TeOperational(start=293.15)
    "Operational (shunt) excitation temperature" annotation (Dialog(group=
          "Operational temperatures", enable=not useThermalPort));
  parameter Real Ne = 1 "Number of turns of excitation winding"
    annotation (Dialog(tab="Excitation"));

  output Modelica.Units.SI.Voltage ve=pin_ep.v - pin_en.v
    "Field excitation voltage";
  output Modelica.Units.SI.Current ie(start=0) = pin_ep.i
    "Field excitation current";
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_ep
    "Positive excitation pin" annotation (Placement(transformation(extent=
           {{-110,70},{-90,50}})));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_en
    "Negative excitation pin" annotation (Placement(transformation(extent=
           {{-90,-50},{-110,-70}})));
  Magnetic_DCMachines.BasicMachines.Components.SinglePhaseWinding excitationWinding(
    final useHeatPort=true,
    final RRef=Re,
    final TRef=TeRef,
    final alpha20=Modelica.Electrical.Machines.Thermal.convertAlpha(alpha20e, TeRef),
    final TOperational(fixed=true),
    final Lsigma=Lesigma,
    final effectiveTurns=Ne,
    final orientation=pi/2) annotation (Placement(transformation(extent={{-90,-20},{-70,0}})));
  // Protected parameters may be deleted
protected
  final parameter Modelica.Units.SI.Inductance Lme=Le*(1 - sigmae)
    "Main part of excitation inductance";
  final parameter Modelica.Units.SI.Inductance Lmezer=Lezer - Lesigma
    "Main part of excitation inductance for small magnetic potential difference";
  final parameter Modelica.Units.SI.Inductance Lmeinf=Leinf - Lesigma
    "Main part of excitation inductance for small magnetic potential difference";
  final parameter Modelica.Units.SI.Inductance Lesigma=Le*sigmae
    "Stray part of excitation inductance" annotation (Evaluate=true);
equation
  assert(Lmeinf>0, "DC_ElectricalExcited: Leinf - sigma * Le must be > 0");
  connect(excitationWinding.pin_p, pin_ep) annotation (Line(points={{-90,0},{-90,60},{-100,60}}, color={0,0,255}));
  connect(excitationWinding.pin_n, pin_en) annotation (Line(points={{-90,-20},{-90,-60},{-100,-60}}, color={0,0,255}));
  connect(excitationWinding.heatPortWinding, internalThermalPort.heatPortExcitation) annotation (Line(points={{-80,-20},{-80,-60},{0,-60},{0,-80}}, color={191,0,0}));
  connect(excitationWinding.port_n,airGap.port_ep)  annotation (Line(points={{-70,-20},{-70,-50},{0,-50},{0,-30}}, color={255,128,0}));
  connect(excitationWinding.port_p,airGap.port_en)  annotation (Line(points={{-70,0},{-60,0},{-60,-30},{-20,-30}}, color={255,128,0}));
  annotation (
    defaultComponentName="dcee",
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{
            100,100}}), graphics={
        Line(points={{-130,-4},{-129,1},{-125,5},{-120,6},{-115,5},{-111,
              1},{-110,-4}}, color={0,0,255}),
        Line(points={{-110,-4},{-109,1},{-105,5},{-100,6},{-95,5},{-91,1},
              {-90,-4}}, color={0,0,255}),
        Line(points={{-90,-4},{-89,1},{-85,5},{-80,6},{-75,5},{-71,1},{-70,
              -4}}, color={0,0,255}),
        Line(points={{-100,-50},{-100,-20},{-70,-20},{-70,-2}}, color={0,0,255}),
        Line(points={{-100,50},{-100,20},{-130,20},{-130,-4}}, color={0,0,
              255})}),
    Documentation(info="<html>
<p><strong>Model of a DC Machine with electrical shunt or separate excitation.</strong><br>
Armature resistance and inductance are modeled directly after the armature pins, then using a <em>AirGapDC</em> model.<br>
The machine models take the following loss effects into account:
</p>

<ul>
<li>heat losses in the temperature dependent armature winding resistance</li>
<li>heat losses in the temperature dependent excitation winding resistance</li>
<li>brush losses in the armature circuit</li>
<li>friction losses</li>
<li>core losses (only eddy current losses, no hysteresis losses)</li>
<li>stray load losses</li>
</ul>

<p>Shunt or separate excitation is defined by the user's external circuit.
<br><strong>Default values for machine's parameters (a realistic example) are:</strong><br></p>
<table>
<tr>
<td>stator's moment of inertia</td>
<td>0.29</td><td>kg.m2</td>
</tr>
<tr>
<td>rotor's moment of inertia</td>
<td>0.15</td><td>kg.m2</td>
</tr>
<tr>
<td>nominal armature voltage</td>
<td>100</td><td>V</td>
</tr>
<tr>
<td>nominal armature current</td>
<td>100</td><td>A</td>
</tr>
<tr>
<td>nominal torque</td>
<td>63.66</td><td>Nm</td>
</tr>
<tr>
<td>nominal speed</td>
<td>1425</td><td>rpm</td>
</tr>
<tr>
<td>nominal mechanical output</td>
<td>9.5</td><td>kW</td>
</tr>
<tr>
<td>efficiency</td>
<td>95.0</td><td>% only armature</td>
</tr>
<tr>
<td>efficiency</td>
<td>94.06</td><td>% including excitation</td>
</tr>
<tr>
<td>armature resistance</td>
<td>0.05</td><td>Ohm at reference temperature</td>
</tr>
<tr>
<td>reference temperature TaRef</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>temperature coefficient alpha20a </td>
<td>0</td><td>1/K</td>
</tr>
<tr>
<td>armature inductance</td>
<td>0.0015</td><td>H</td>
</tr>
<tr>
<td>nominal excitation voltage</td>
<td>100</td><td>V</td>
</tr>
<tr>
<td>nominal excitation current</td>
<td>1</td><td>A</td>
</tr>
<tr>
<td>excitation resistance</td>
<td>100</td><td>Ohm at reference temperature</td>
</tr>
<tr>
<td>reference temperature TeRef</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>temperature coefficient alpha20e </td>
<td>0</td><td>1/K</td>
</tr>
<tr>
<td>excitation inductance</td>
<td>1</td><td>H</td>
</tr>
<tr>
<td>stray part of excitation inductance</td>
<td>0</td><td> </td>
</tr>
<tr>
<td>armature nominal temperature TaNominal</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>armature operational temperature TaOperational</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>(shunt) excitation operational temperature TeOperational</td>
<td>20</td><td>&deg;C</td>
</tr>
</table>
<p>
Armature resistance resp. inductance include resistance resp. inductance of commutating pole winding and
compensation winding, if present.<br>
Armature current does not cover excitation current of a shunt excitation; in this case total current drawn from the grid = armature current + excitation current.
</p>
<h4>Note:</h4>
<p>
Armature current does not cover excitation current of a shunt excitation; in this case total current drawn from the grid = armature current + excitation current.
</p>
<p>
The inner voltage of the series excited machine is calculated as follows <code>Vi = Va - (Ra + Re)*Ia - brushVoltageDrop = turnsRatio*psi_e*wMechanical</code>
</p>
</html>"));
end DC_ElectricalExcited;
