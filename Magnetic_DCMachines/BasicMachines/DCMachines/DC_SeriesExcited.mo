within Magnetic_DCMachines.BasicMachines.DCMachines;
model DC_SeriesExcited "Series excited linear DC machine"
  import Modelica.Constants.pi;
  extends Magnetic_DCMachines.BaseClasses.DCMachine(
    wNominal(start=1410*2*pi/60),
    final ViNominal=VaNominal - (Modelica.Electrical.Machines.Thermal.convertResistance(
        Ra,
        TaRef,
        alpha20a,
        TaNominal) + Modelica.Electrical.Machines.Thermal.convertResistance(
        Rs,
        TsRef,
        alpha20s,
        TsNominal))*IaNominal - Modelica.Electrical.Machines.Losses.DCMachines.brushVoltageDrop(brushParameters, IaNominal),
    final PhiNominal=Lms*abs(IaNominal)/Ns,
    redeclare final Modelica.Electrical.Machines.Thermal.DCMachines.ThermalAmbientDCSE thermalAmbient(final Tse=TsOperational),
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.ThermalPortDCSE thermalPort,
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.ThermalPortDCSE internalThermalPort,
    redeclare final Modelica.Electrical.Machines.Interfaces.DCMachines.PowerBalanceDCSE powerBalance(final powerSeriesExcitation=vs*is, final lossPowerSeriesExcitation=seriesExcitationWinding.resistor.LossPower),
    airGap(final useSaturation=useSaturation,
      final G_me=Lms/Ns^2,
      final G_mezer=Lmszer/Ns^2,
      final G_meinf=Lmsinf/Ns^2,
      final V_menom=Ns*IaNominal));
  parameter Modelica.Units.SI.Resistance Rs(start=0.01)
    "Series excitation resistance at TsRef"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Temperature TsRef(start=293.15)
    "Reference temperature of series excitation resistance"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Electrical.Machines.Thermal.LinearTemperatureCoefficient20
    alpha20s(start=0) "Temperature coefficient of series excitation resistance"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Inductance Ls(start=0.0005)
    "Total series excitation inductance" annotation (Dialog(tab="Excitation"));
  parameter Real sigmas(
    final min=0,
    final max=0.99,
    start=0) "Stray fraction of series excitation inductance"
    annotation (Dialog(tab="Excitation"));
  parameter Boolean useSaturation=false "Take saturation into account?"
    annotation (Dialog(tab="Excitation"));
  parameter Modelica.Units.SI.Inductance Lszer=5*Ls
    "Inductance at small current"
    annotation (Dialog(tab="Excitation", enable=useSaturation));
  parameter Modelica.Units.SI.Inductance Lsinf=Ls/5
    "Inductance at large current"
    annotation (Dialog(tab="Excitation", enable=useSaturation));
  parameter Modelica.Units.SI.Temperature TsNominal(start=293.15)
    "Nominal series excitation temperature"
    annotation (Dialog(tab="Nominal parameters"));
  parameter Modelica.Units.SI.Temperature TsOperational(start=293.15)
    "Operational series excitation temperature" annotation (Dialog(group=
          "Operational temperatures", enable=not useThermalPort));
  parameter Real Ns = 1 "Number of turns of series excitation winding"
    annotation (Dialog(tab="Excitation"));

  output Modelica.Units.SI.Voltage vs=pin_sp.v - pin_sn.v
    "Series excitation voltage";
  output Modelica.Units.SI.Current is=pin_sp.i "Series excitation current";
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_sp
    "Positive series excitation pin" annotation (Placement(transformation(
          extent={{-110,70},{-90,50}})));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_sn
    "Negative series excitation pin" annotation (Placement(transformation(
          extent={{-90,-50},{-110,-70}})));
  Magnetic_DCMachines.BasicMachines.Components.SinglePhaseWinding seriesExcitationWinding(
    final useHeatPort=true,
    final RRef=Rs,
    final TRef=TsRef,
    final alpha20=Modelica.Electrical.Machines.Thermal.convertAlpha(alpha20s, TsRef),
    final TOperational(fixed=true),
    final Lsigma=Lssigma,
    final effectiveTurns=Ns,
    orientation=pi/2) annotation (Placement(transformation(extent={{-90,-20},{-70,0}})));
protected
  final parameter Modelica.Units.SI.Inductance Lms=Ls*(1 - sigmas)
    "Main part of series excitation inductance";
  final parameter Modelica.Units.SI.Inductance Lmszer=Lszer - Lssigma
    "Main part of excitation inductance for small magnetic potential difference";
  final parameter Modelica.Units.SI.Inductance Lmsinf=Lsinf - Lssigma
    "Main part of excitation inductance for small magnetic potential difference";
  final parameter Modelica.Units.SI.Inductance Lssigma=Ls*sigmas
    "Stray part of excitation inductance" annotation (Evaluate=true);
equation
  assert(Lmsinf>0, "DC_ElectricalExcited: Lsinf - sigma * Le must be > 0");
  connect(seriesExcitationWinding.pin_p, pin_sp) annotation (Line(points={{-90,0},{-90,60},{-100,60}}, color={0,0,255}));
  connect(seriesExcitationWinding.pin_n, pin_sn) annotation (Line(points={{-90,-20},{-90,-60},{-100,-60}}, color={0,0,255}));
  connect(seriesExcitationWinding.heatPortWinding, internalThermalPort.heatPortSeriesExcitation) annotation (Line(points={{-80,-20},{-80,-60},{0,-60},{0,-80}}, color={191,0,0}));
  connect(seriesExcitationWinding.port_n,airGap.port_ep)  annotation (Line(points={{-70,-20},{-70,-50},{0,-50},{0,-30}}, color={255,128,0}));
  connect(seriesExcitationWinding.port_p,airGap.port_en)  annotation (Line(points={{-70,0},{-60,0},{-60,-30},{-20,-30}}, color={255,128,0}));
  annotation (
    defaultComponentName="dcse",
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{
            100,100}}), graphics={
        Line(points={{-100,-10},{-105,-9},{-109,-5},{-110,0},{-109,5},{-105,
              9},{-100,10}}, color={0,0,255}),
        Line(points={{-100,-30},{-105,-29},{-109,-25},{-110,-20},{-109,-15},
              {-105,-11},{-100,-10}}, color={0,0,255}),
        Line(points={{-100,10},{-105,11},{-109,15},{-110,20},{-109,25},{-105,
              29},{-100,30}}, color={0,0,255}),
        Line(points={{-100,50},{-100,30}}, color={0,0,255}),
        Line(points={{-100,-30},{-100,-50}}, color={0,0,255})}),
    Documentation(info="<html>
<p><strong>Model of a DC Machine with series excitation.</strong><br>
Armature resistance and inductance are modeled directly after the armature pins, then using a <em>AirGapDC</em> model.<br>
The machine models take the following loss effects into account:
</p>

<ul>
<li>heat losses in the temperature dependent armature winding resistance</li>
<li>heat losses in the temperature dependent excitation winding resistance</li>
<li>brush losses in the armature circuit</li>
<li>friction losses</li>
<li>core losses (only eddy current losses, no hysteresis losses)</li>
<li>stray load losses</li>
</ul>

<p>Series excitation has to be connected by the user's external circuit.
<br><strong>Default values for machine's parameters (a realistic example) are:</strong><br></p>
<table>
<tr>
<td>stator's moment of inertia</td>
<td>0.29</td><td>kg.m2</td>
</tr>
<tr>
<td>rotor's moment of inertia</td>
<td>0.15</td><td>kg.m2</td>
</tr>
<tr>
<td>nominal armature voltage</td>
<td>100</td><td>V</td>
</tr>
<tr>
<td>nominal armature current</td>
<td>100</td><td>A</td>
</tr>
<tr>
<td>nominal torque</td>
<td>63.66</td><td>Nm</td>
</tr>
<tr>
<td>nominal speed</td>
<td>1410</td><td>rpm</td>
</tr>
<tr>
<td>nominal mechanical output</td>
<td>9.4</td><td>kW</td>
</tr>
<tr>
<td>efficiency</td>
<td>94.0</td><td>% only armature</td>
</tr>
<tr>
<td>armature resistance</td>
<td>0.05</td><td>Ohm at reference temperature</td>
</tr>
<tr>
<td>reference temperature TaRef</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>temperature coefficient alpha20a </td>
<td>0</td><td>1/K</td>
</tr>
<tr>
<td>armature inductance</td>
<td>0.0015</td><td>H</td>
</tr>
<tr>
<td>excitation resistance</td>
<td>0.01</td><td>Ohm at reference temperature</td>
</tr>
<tr>
<td>reference temperature TsRef</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>temperature coefficient alpha20s</td>
<td>0</td><td>1/K</td>
</tr>
<tr>
<td>excitation inductance</td>
<td>0.0005</td><td>H</td>
</tr>
<tr>
<td>stray part of excitation inductance</td>
<td>0</td><td> </td>
</tr>
<tr>
<td>armature nominal temperature TaNominal</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>series excitation nominal temperature TsNominal</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>armature operational temperature TaOperational</td>
<td>20</td><td>&deg;C</td>
</tr>
<tr>
<td>series excitation operational temperature TsOperational</td>
<td>20</td><td>&deg;C</td>
</tr>
</table>
<p>
Armature resistance resp. inductance include resistance resp. inductance of commutating pole winding and
compensation winding, if present.<br>
Parameter nominal armature voltage includes voltage drop of series excitation;<br>
but for output the voltage is split into:<br>
va = armature voltage without voltage drop of series excitation<br>
vs = voltage drop of series excitation
</p>
<h4>Note:</h4>
<p>
The excitation pins are separated from the armature pins to be able to reverse either armature current or excitation current independent of the other in order to reverse directon of rotation.
</p>
<p>
The inner voltage of the series excited machine is calculated as follows <code>Vi = Va - (Ra + Rs)*Ia - brushVoltageDrop = turnsRatio*psi_e*wMechanical</code>
</p>
</html>"));
end DC_SeriesExcited;
