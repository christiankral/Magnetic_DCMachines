within Magnetic_DCMachines.MoveTo_Modelica;
model SaturatingInductor "Simple model of an inductor with saturation"
  extends Modelica.Electrical.Analog.Interfaces.OnePort(i(start=0));
  import Magnetic_DCMachines.Saturation.Functions.saturatedFlux;
  parameter Saturation.ParameterRecords.SaturationData saturationData "Parameters of saturated inductance" annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.Units.SI.MagneticFlux psi "Actual flux";
protected
  final parameter Boolean useSaturation=saturationData.useSaturation;
equation
  psi = saturatedFlux(saturationData, i);
  v = der(psi);
  annotation (defaultComponentName="inductor",
    Icon(coordinateSystem(preserveAspectRatio=true, extent={{-100,-100},{100,
            100}}), graphics={
        Line(points={{60,0},{90,0}}, color={0,0,255}),
        Line(points={{-90,0},{-60,0}}, color={0,0,255}),
        Line(
          points={{-60,0},{-59,6},{-52,14},{-38,14},{-31,6},{-30,0}},
          color={0,0,255},
          smooth=Smooth.Bezier),
        Line(
          points={{-30,0},{-29,6},{-22,14},{-8,14},{-1,6},{0,0}},
          color={0,0,255},
          smooth=Smooth.Bezier),
        Line(
          points={{0,0},{1,6},{8,14},{22,14},{29,6},{30,0}},
          color={0,0,255},
          smooth=Smooth.Bezier),
        Line(
          points={{30,0},{31,6},{38,14},{52,14},{59,6},{60,0}},
          color={0,0,255},
          smooth=Smooth.Bezier),
        Text(
          extent={{-150,90},{150,50}},
          textString="%name",
          textColor={0,0,255}),
        Rectangle(visible=useSaturation,
          extent={{-60,-10},{60,-20}},
          fillPattern=FillPattern.Sphere,
          fillColor={0,0,255})}),
    Documentation(info="<html>
<p>
This model approximates the behaviour of an inductor under the influence of saturation.
The value of the inductance depends on the current flowing through the inductor, based on an approximation based on the atan-function.
The inductance decreases as current increases.
</p>
<p>
For special usage in quasiStatic DC machines, the voltage drop <code>v</code> is set to <code>0</code>, if <code>constant Boolean quasiStatic = true</code> (default is <code>quasiStatic = false</code>).
</p>
</html>",
        revisions="<html>
<dl>
  <dt><strong>Main Author:</strong></dt>
  <dd>
  <a href=\"https://www.haumer.at/\">Anton Haumer</a><br>
  Technical Consulting &amp; Electrical Engineering<br>
  D-93049 Regensburg<br>Germany<br>
  email: <a href=\"mailto:a.haumer@haumer.at\">a.haumer@haumer.at</a>
  </dd>
  <dt><strong>Release Notes:</strong></dt>
  <dd>2019-07-19: Improved iteration of parameter Ipar</dd>
  <dd>2004-03-27: 1st implementation</dd>
 </dl>
</html>"));
end SaturatingInductor;
