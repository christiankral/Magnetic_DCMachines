within Magnetic_DCMachines.Components;
model Reluctance "Symmetric reluctance"
  extends Modelica.Magnetic.FundamentalWave.Interfaces.TwoPort;
  parameter Modelica.Units.SI.Reluctance R_m(start=1)
    "Symmetric magnetic reluctance";
equation
  V_m.re = R_m * Phi.re;
  V_m.im = R_m * Phi.im;
  annotation (Icon(coordinateSystem(preserveAspectRatio=false, extent={{-100,
            -100},{100,100}}), graphics={Rectangle(
              extent={{-70,30},{70,-30}},
              lineColor={255,128,0},
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),Line(points={{-96,0},{-70,0}},
          color={255,128,0}),Line(points={{70,0},{96,0}}, color={255,128,0}),
          Text(
              extent={{-150,50},{150,90}},
              lineColor={0,0,255},
              textString="%name")}), Documentation(info="<html>
<p>
The salient reluctance models the relationship between the complex magnetic potential difference
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/V_m.png\" alt=\"V_m.png\"> and the complex magnetic flux <img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Phi.png\">,
</p>

<blockquote>
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Components/reluctance.png\"
      alt=\"reluctance.png\">
</blockquote>

<p>which can also be expressed in terms complex phasors:</p>

<blockquote>
<img src=\"modelica://Modelica/Resources/Images/Magnetic/FundamentalWave/Components/reluctance_alt.png\"
     alt=\"reluctance_alt.png\">
</blockquote>

</html>"));
end Reluctance;
