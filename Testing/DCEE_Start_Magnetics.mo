within Testing;
model DCEE_Start_Magnetics "Test example: DC with electrical excitation starting with voltage ramp"
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  parameter Modelica.SIunits.Voltage Va=100 "Actual armature voltage";
  parameter Modelica.SIunits.Time tStart=0.2
    "Start of armature voltage ramp";
  parameter Modelica.SIunits.Time tRamp=0.8 "Armature voltage ramp";
  parameter Modelica.SIunits.Voltage Ve=100 "Actual excitation voltage";
  parameter Modelica.SIunits.Torque TLoad=63.66 "Nominal load torque";
  parameter Modelica.SIunits.Time tStep=1.5 "Time of load torque step";
  parameter Modelica.SIunits.Inertia JLoad=0.15
    "Load's moment of inertia";

  Magnetic_DCMachines.BasicMachines.DCMachines.DC_ElectricalExcited dcee(
    VaNominal=dceeData.VaNominal,
    IaNominal=dceeData.IaNominal,
    wNominal=dceeData.wNominal,
    TaNominal=dceeData.TaNominal,
    Ra=dceeData.Ra,
    TaRef=dceeData.TaRef,
    La=dceeData.La,
    Jr=dceeData.Jr,
    useSupport=false,
    Js=dceeData.Js,
    frictionParameters=dceeData.frictionParameters,
    coreParameters=dceeData.coreParameters,
    strayLoadParameters=dceeData.strayLoadParameters,
    brushParameters=dceeData.brushParameters,
    IeNominal=dceeData.IeNominal,
    Re=dceeData.Re,
    TeRef=dceeData.TeRef,
    Le=dceeData.Le,
    sigmae=dceeData.sigmae,
    TaOperational=293.15,
    alpha20a=dceeData.alpha20a,
    alpha20e=dceeData.alpha20e,
    TeOperational=293.15,
    Ne=100) annotation (Placement(transformation(extent={{-20,-50},{0,-30}})));
  Modelica.Blocks.Sources.Ramp ramp(
    duration=tRamp,
    height=Va,
    startTime=tStart) annotation (Placement(transformation(extent={{-80,
            60},{-60,80}})));
  Modelica.Electrical.Analog.Sources.SignalVoltage signalVoltage
    annotation (Placement(transformation(extent={{0,30},{-20,50}})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        transformation(
        origin={-70,40},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V=Ve)
    annotation (Placement(transformation(
        origin={-60,-40},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Modelica.Electrical.Analog.Basic.Ground groundExcitation annotation (
      Placement(transformation(
        origin={-90,-50},
        extent={{-10,-10},{10,10}},
        rotation=270)));
  Modelica.Mechanics.Rotational.Components.Inertia loadInertia(J=JLoad)
    annotation (Placement(transformation(extent={{40,-50},{60,-30}})));
  Modelica.Mechanics.Rotational.Sources.TorqueStep loadTorqueStep(
    startTime=tStep,
    stepTorque=-TLoad,
    useSupport=false,
    offsetTorque=0) annotation (Placement(transformation(extent={{90,-50},{70,-30}})));
  parameter Magnetic_DCMachines.Utilities.ParameterRecords.DcElectricalExcitedData dceeData(Jr=0, La=1E-6)
                                                                                            annotation (Placement(transformation(extent={{60,60},{80,80}})));
  Modelica.Electrical.Machines.BasicMachines.DCMachines.DC_ElectricalExcited dcee_e(
    VaNominal=dceeData.VaNominal,
    IaNominal=dceeData.IaNominal,
    wNominal=dceeData.wNominal,
    TaNominal=dceeData.TaNominal,
    Ra=dceeData.Ra,
    TaRef=dceeData.TaRef,
    La=dceeData.La,
    Jr=dceeData.Jr,
    useSupport=false,
    Js=dceeData.Js,
    frictionParameters=dceeData.frictionParameters,
    coreParameters=dceeData.coreParameters,
    strayLoadParameters=dceeData.strayLoadParameters,
    brushParameters=dceeData.brushParameters,
    IeNominal=dceeData.IeNominal,
    Re=dceeData.Re,
    TeRef=dceeData.TeRef,
    Le=dceeData.Le,
    sigmae=dceeData.sigmae,
    TaOperational=293.15,
    alpha20a=dceeData.alpha20a,
    alpha20e=dceeData.alpha20e,
    TeOperational=293.15) annotation (Placement(transformation(extent={{-20,-100},{0,-80}})));
  Modelica.Mechanics.Rotational.Components.Inertia loadInertia_e(J=JLoad) annotation (Placement(transformation(extent={{40,-100},{60,-80}})));
  Modelica.Mechanics.Rotational.Sources.TorqueStep loadTorqueStep_e(
    startTime=tStep,
    stepTorque=-TLoad,
    useSupport=false,
    offsetTorque=0) annotation (Placement(transformation(extent={{90,-100},{70,-80}})));
equation
  connect(ramp.y, signalVoltage.v) annotation (Line(points={{-59,70},{-10,70},{-10,52}},
                         color={0,0,255}));
  connect(signalVoltage.p, dcee.pin_ap) annotation (Line(points={{0,40},{0,-20},{-4,-20},{-4,-30}},
                                     color={0,0,255}));
  connect(signalVoltage.n, ground.p)
    annotation (Line(points={{-20,40},{-60,40}}, color={0,0,255}));
  connect(dcee.pin_an, ground.p) annotation (Line(points={{-16,-30},{-16,-20},{-20,-20},{-20,40},{-60,40}},
                                             color={0,0,255}));
  connect(constantVoltage.n, groundExcitation.p)
    annotation (Line(points={{-60,-50},{-80,-50}}, color={0,0,255}));
  connect(dcee.pin_ep, constantVoltage.p) annotation (Line(points={{-20,-34},{-40,-34},{-40,-30},{-60,-30}},
                                          color={0,0,255}));
  connect(dcee.pin_en, constantVoltage.n) annotation (Line(points={{-20,-46},{-40,-46},{-40,-50},{-60,-50}},
                                          color={0,0,255}));
  connect(loadInertia.flange_b, loadTorqueStep.flange)
    annotation (Line(points={{60,-40},{70,-40}}));
  connect(dcee.flange, loadInertia.flange_a) annotation (Line(
      points={{0,-40},{40,-40}}));
  connect(dcee_e.pin_ap, dcee.pin_ap) annotation (Line(points={{-4,-80},{-4,-70},{8,-70},{8,-30},{-4,-30}}, color={0,0,255}));
  connect(dcee_e.pin_an, dcee.pin_an) annotation (Line(points={{-16,-80},{-16,-70},{-26,-70},{-26,-30},{-16,-30}}, color={0,0,255}));
  connect(dcee.pin_ep, dcee_e.pin_ep) annotation (Line(points={{-20,-34},{-28,-34},{-28,-32},{-34,-32},{-34,-84},{-20,-84}}, color={0,0,255}));
  connect(dcee_e.pin_en, constantVoltage.n) annotation (Line(points={{-20,-96},{-40,-96},{-40,-50},{-60,-50}}, color={0,0,255}));
  connect(loadInertia_e.flange_b, loadTorqueStep_e.flange) annotation (Line(points={{60,-90},{70,-90}}));
  connect(dcee_e.flange, loadInertia_e.flange_a) annotation (Line(points={{0,-90},{40,-90}}, color={0,0,0}));
  annotation (experiment(
      StopTime=2,
      Interval=0.0001,
      Tolerance=1e-08),                                                Documentation(
        info="<html>
<strong>Test example: Electrically separate excited DC machine started with an armature voltage ramp</strong><br>
A voltage ramp is applied to the armature, causing the DC machine to start,
and accelerating inertias.<br>At time tStep a load step is applied.<br>
Simulate for 2 seconds and plot (versus time):
<ul>
<li>dcee.ia: armature current</li>
<li>dcee.wMechanical: motor's speed</li>
<li>dcee.tauElectrical: motor's torque</li>
<li>dcee.ie: excitation current</li>
</ul>
Default machine parameters of model <em>DC_ElectricalExcited</em> are used.
</html>"));
end DCEE_Start_Magnetics;
