within ;
package Testing
  extends Modelica.Icons.Package;

  annotation (uses(Modelica(version="3.2.3"), Magnetic_DCMachines(version="0.X.X")));
end Testing;
